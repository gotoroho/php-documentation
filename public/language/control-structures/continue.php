<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/control-structures.php">Назад</a></li>
</ul>

<h1>continue</h1>

<p>continue используется внутри циклических структур для пропуска оставшейся части текущей итерации цикла и, при соблюдении условий, начала следующей итерации.</p>

<p><b>Замечание:</b> В PHP оператор switch считается циклическим и внутри него может использоваться continue. Если continue не передано аргументов, то он ведёт себя аналогично break, но выдаёт предупреждение о возможной ошибке. Если switch расположен внутри цикла, continue 2 продолжит выполнение внешнего цикла со следующей итерации.</p>

<p>continue принимает необязательный числовой аргумент, который указывает на скольких уровнях вложенных циклов будет пропущена оставшаяся часть итерации. Значением по умолчанию является 1, при которой пропускается оставшаяся часть текущего цикла.</p>

<p>continue внутри switch, использующееся как замена break для switch будет вызывать ошибку уровня E_WARNING.</p>

</body>
</html>
