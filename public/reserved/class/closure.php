<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/interfaces.php">Назад</a></li>
</ul>

<h1>Класс Closure</h1>

<p>Класс, используемый для создания анонимных функций.</p>

<p>Анонимные функции выдают объекты этого типа. Класс получил методы, позволяющие контролировать анонимную функцию после её создания.</p>

<p>Кроме методов, описанных здесь, этот класс также имеет метод __invoke. Данный метод необходим только для совместимости с другими классами, в которых реализован магический вызов, так как этот метод не используется при вызове функции.</p>

<ul>
    <li>Closure::__construct — Конструктор, запрещающий создание экземпляра</li>
    <li>Closure::bind — Дублирует замыкание с указанием конкретного связанного объекта и области видимости класса</li>
    <li>Closure::bindTo — Дублирует замыкание с указанием связанного объекта и области видимости класса</li>
    <li>Closure::call — Связывает и запускает замыкание</li>
    <li>Closure::fromCallable — Конвертирует callable в замыкание</li>
</ul>

</body>
</html>
