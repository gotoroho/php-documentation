<?php

function test_global_ref()
{
    global $obj;
    $new = new stdclass;
    $obj = &$new;
}

function test_global_noref()
{
    global $obj;
    $new = new stdclass;
    $obj = $new;
}

test_global_ref();
var_dump($obj); // NULL
test_global_noref();
var_dump($obj); // object(stdClass)#1 (0) {}
