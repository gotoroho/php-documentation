<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/variables.php">Назад</a></li>
</ul>

<h1>$http_response_header</h1>

<p>Массив (array) $http_response_header похож на функцию get_headers(). При использовании обёртки HTTP, $http_response_header будет заполняться заголовками ответа HTTP. $http_response_header будет создан в локальной области видимости.</p>

</body>
</html>
