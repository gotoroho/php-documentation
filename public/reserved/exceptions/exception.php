<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/exceptions.php">Назад</a></li>
</ul>

<h1>Exception</h1>

<p>Exception — это базовый класс для всех пользовательских исключений.</p>

<pre>
    class Exception implements Throwable {
        /* Свойства */
        protected string $message; // Текст исключения
        protected int $code; // Код исключения
        protected string $file; // Имя файла, в котором было вызвано исключение
        protected int $line; // Номер строки, в которой было вызвано исключение

        /* Методы */
        public __construct(string $message = "", int $code = 0, ?Throwable $previous = null) // Создать исключение
        final public getMessage(): string // Получает сообщение исключения
        final public getPrevious(): ?Throwable // Возвращает предыдущий объект, реализующий Throwable
        final public getCode(): int // Получает код исключения
        final public getFile(): string // Получает файл, в котором возникло исключение
        final public getLine(): int // Получает строку, в которой возникло исключение
        final public getTrace(): array // Получает трассировку стека
        final public getTraceAsString(): string // Получает трассировку стека в виде строки
        public __toString(): string // Строковое представление исключения
        final private __clone(): void // Клонировать исключение
    }
</pre>

</body>
</html>
