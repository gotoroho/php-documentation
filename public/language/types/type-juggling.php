<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/types.php">Назад</a></li>
</ul>

<h1>Манипуляции с типами</h1>

<p>PHP не требует и не поддерживает явного типа при определении переменной, он определяется по контексту.</p>

<p>* (умножение) преобразовывает в float</p>

<p><b>Замечание:</b> Поведение автоматического преобразования в массив в настоящий момент не определено.</p>

<pre>
    &lt;?php
    $a    = 'car'; // $a - это строка
    $a[0] = 'b';   // $a всё ещё строка
    echo $a;       // bar
    ?&gt;
</pre>

<h2>Приведения</h2>

<p>Приводится как в Си, словом в скобках.</p>

<ul>
    <li>(int), (integer) - приведение к int</li>
    <li>(bool), (boolean) - приведение к bool</li>
    <li>(float), (double), (real) - приведение к float</li>
    <li>(string) - приведение к string</li>
    <li>(array) - приведение к array</li>
    <li>(object) - приведение к object</li>
    <li>(unset) - приведение к NULL</li>
</ul>

<p>Приведение типа (binary) и поддержка префикса b существует для прямой поддержки. Обратите внимание, что (binary) по существу то же самое, что и (string), но не следует полагаться на этот тип приведения.</p>

<p>Приведение типа (unset) объявлено устаревшим с PHP 7.2.0. Обратите внимание, что приведение типа (unset) это то же самое, что присвоение NULL переменной. Тип приведения (unset) удалён в PHP 8.0.0.</p>

</body>
</html>
