<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/interfaces.php">Назад</a></li>
</ul>

<h1>Класс Generator</h1>

<p>Создание объектов типа Generator описано в разделе Генераторы.</p>

<p>Предостережение Объекты Generator не могут быть созданы с помощью оператора new.</p>

<ul>
    <li>Generator::current — Получить текущее значение генератора</li>
    <li>Generator::getReturn — Получить значение, возвращаемое генератором</li>
    <li>Generator::key — Получить ключ сгенерированного элемента</li>
    <li>Generator::next — Возобновить работу генератора</li>
    <li>Generator::rewind — Перемотать итератор</li>
    <li>Generator::send — Передать значение в генератор</li>
    <li>Generator::throw — Бросить исключение в генератор</li>
    <li>Generator::valid — Проверка, закрыт ли итератор</li>
    <li>Generator::__wakeup — Callback-функция сериализации</li>
</ul>

</body>
</html>
