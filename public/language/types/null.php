<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/types.php">Назад</a></li>
</ul>

<h1>Null</h1>

<p>null - это единственно возможное значение типа null.</p>

<figure>
    <figcaption>Переменная null, если</figcaption>
    <ul>
        <li>ей была присвоена константа null.</li>
        <li>ей ещё не было присвоено никакого значения.</li>
        <li>она была удалена с помощью unset().</li>
    </ul>
</figure>

</body>
</html>
