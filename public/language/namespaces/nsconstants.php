<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/namespaces.php">Назад</a></li>
</ul>

<h1>Ключевое слово namespace и константа __NAMESPACE__</h1>

<p>PHP поддерживает два способа доступа к абстрактным элементам в текущем пространстве имён таким, как магическая константа __NAMESPACE__ и ключевое слово namespace.</p>

<p>Значение константы __NAMESPACE__ - это строка, которая содержит имя текущего пространства имён. В глобальном пространстве, вне пространства имён, она содержит пустую строку.</p>

<p>Ключевое слово namespace может быть использовано для явного запроса элемента из текущего пространства имён или из подпространства. Это эквивалент оператора self для классов в пространстве имён.</p>

<pre>
    &lt;?php
    blah\mine(); // вызывает функцию MyProject\blah\mine()
    namespace\blah\mine(); // вызывает функцию MyProject\blah\mine()
    ?&gt;
</pre>

</body>
</html>
