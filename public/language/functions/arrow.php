<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/functions.php">Назад</a></li>
</ul>

<h1>Стрелочные функции</h1>

<p>Стрелочные функции появились в PHP 7.4, как более лаконичный синтаксис для анонимных функций.</p>

<p>И анонимные, и стрелочные функции реализованы с использованием класса Closure.</p>

<p>Основной вид записи стрелочных функций: fn (argument_list) => expr.</p>

<p>Стрелочные функции поддерживают те же возможности, что и анонимные функции, за исключением того, что использование переменных из родительской области всегда выполняется автоматически.</p>

<p>Когда переменная, используемая в выражении, определена в родительской области, она будет неявно захвачена по значению. В следующем примере функции $fn1 и $fn2 ведут себя одинаково.</p>

<pre>
    &lt;?php

    $y = 1;

    $fn1 = fn($x) => $x + $y;
    // эквивалентно использованию $y по значению:
    $fn2 = function ($x) use ($y) {
        return $x + $y;
    };

    var_export($fn1(3));
    ?&gt;
</pre>

<p>Это также работает для вложенных стрелочных функций.</p>

<p>Подобно анонимным функциям, синтаксис стрелочных функций допускает произвольные сигнатуры функций, включая типы параметров и возвращаемых значений, значения по умолчанию, переменные, а также передачу и возврат по ссылке. Ниже приведены корректные примеры стрелочных функций:</p>

<pre>
    &lt;?php

    fn(array $x) => $x;
    static fn(): int => $x;
    fn($x = 42) => $x;
    fn(&$x) => $x;
    fn&($x) => $x;
    fn($x, ...$rest) => $rest;

    ?&gt;
</pre>

<p>Стрелочные функции используют привязку переменных по значению. Это примерно эквивалентно выполнению use($x) для каждой переменной $x, используемой внутри стрелочной функции. Привязка по значению означает, что невозможно изменить какие-либо значения из внешней области. Вместо этого можно использовать анонимные функции для привязок по ссылкам.</p>

<p><b>Замечание:</b> Можно использовать func_num_args(), func_get_arg() и func_get_args() в стрелочной функции.</p>

</body>
</html>
