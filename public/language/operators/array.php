<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/operators.php">Назад</a></li>
</ul>

<h1>Операторы, работающие с массивами</h1>

<table>
    <caption><strong>Операторы, работающие с массивами</strong></caption>

    <thead>
    <tr>
        <th>Пример</th>
        <th>Название</th>
        <th>Результат</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>$a + $b</td>
        <td>Объединение</td>
        <td>Объединение массива $a и массива $b.</td>
    </tr>
    <tr>
        <td>$a == $b</td>
        <td>Равно</td>
        <td>true в случае, если $a и $b содержат одни и те же пары ключ/значение.</td>
    </tr>
    <tr>
        <td>$a === $b</td>
        <td>Тождественно равно</td>
        <td>true в случае, если $a и $b содержат одни и те же пары ключ/значение в том же самом порядке и того же типа.</td>
    </tr>
    <tr>
        <td>$a != $b</td>
        <td>Не равно</td>
        <td>true, если массив $a не равен массиву $b.</td>
    </tr>
    <tr>
        <td>$a <> $b</td>
        <td>Не равно</td>
        <td>true, если массив $a не равен массиву $b.</td>
    </tr>
    <tr>
        <td>$a !== $b</td>
        <td>Тождественно не равно</td>
        <td>true, если массив $a не равен тождественно массиву $b.</td>
    </tr>
    </tbody>
</table>

<p>Оператор + возвращает левый массив, к которому был присоединён правый массив. Для ключей, которые существуют в обоих массивах, будут использованы значения из левого массива, а соответствующие им элементы из правого массива будут проигнорированы.</p>



</body>
</html>
