<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Позднее статическое связывание</h1>

<p>PHP реализует функцию, называемую позднее статическое связывание, которая может быть использована для того, чтобы получить ссылку на вызываемый класс в контексте статического наследования.</p>

<p>Если говорить более точно, позднее статическое связывание сохраняет имя класса указанного в последнем "неперенаправленном вызове". В случае статических вызовов это явно указанный класс (обычно слева от оператора ::); в случае не статических вызовов это класс объекта. "Перенаправленный вызов" - это статический вызов, начинающийся с self::, parent::, static::, или, если двигаться вверх по иерархии классов, forward_static_call(). Функция get_called_class() может быть использована для получения строки с именем вызванного класса, а static:: представляет её область действия.</p>

<p>Само название "позднее статическое связывание" отражает в себе внутреннюю реализацию этой особенности. "Позднее связывание" отражает тот факт, что обращения через static:: не будут вычисляться по отношению к классу, в котором вызываемый метод определён, а будут вычисляться на основе информации в ходе исполнения. Также эта особенность была названа "статическое связывание" потому, что она может быть использована (но не обязательно) в статических методах.</p>

<h2>Ограничения self::</h2>

<pre>
    &lt;?php
    class A {
        public static function who() {
            echo __CLASS__;
        }
        public static function test() {
            self::who();
        }
    }

    class B extends A {
        public static function who() {
            echo __CLASS__;
        }
    }

    B::test(); // A
    ?&gt;
</pre>

<h2>Использование позднего статического связывания</h2>

<p>Позднее статическое связывание пытается устранить это ограничение, предоставляя ключевое слово, которое ссылается на класс, вызванный непосредственно в ходе выполнения. Попросту говоря, ключевое слово, которое позволит вам ссылаться на B из test() в предыдущем примере. Было решено не вводить новое ключевое слово, а использовать static, которое уже зарезервировано.</p>

<pre>
    &lt;?php
    class A {
        public static function who() {
            echo __CLASS__;
        }
        public static function test() {
            static::who(); // Здесь действует позднее статическое связывание
        }
    }

    class B extends A {
        public static function who() {
            echo __CLASS__;
        }
    }

    B::test(); // B
    ?&gt;
</pre>

<p><b>Замечание:</b> В нестатическом контексте вызванным классом будет тот, к которому относится экземпляр объекта. Поскольку $this-> будет пытаться вызывать закрытые методы из той же области действия, использование static:: может дать разные результаты. Другое отличие в том, что static:: может ссылаться только на статические поля класса.</p>

<pre>
    &lt;?php
    class A {
        private function foo() {
            echo "success!\n";
        }
        public function test() {
            $this->foo();
            static::foo();
        }
    }

    class B extends A {
        /* foo() будет скопирован в В, следовательно его область действия по прежнему А,
           и вызов будет успешным */
    }

    class C extends A {
        private function foo() {
            /* исходный метод заменён; область действия нового метода - С */
        }
    }

    $b = new B();
    $b->test();
    $c = new C();
    $c->test();   // потерпит ошибку
    ?&gt;
</pre>

<small>
    success!<br>
    success!<br>
    success!<br>
    Fatal error:  Call to private method C::foo() from context 'A' in /tmp/test.php on line 9
</small>

<p><b>Замечание:</b> Разрешающая область позднего статического связывания будет фиксирована вычисляющем её статическим вызовом. С другой стороны, статические вызовы с использованием таких директив как parent:: или self:: перенаправляют информацию вызова.</p>

<pre>
    &lt;?php
    class A {
        public static function foo() {
            static::who();
        }

        public static function who() {
            echo __CLASS__."\n";
        }
    }

    class B extends A {
        public static function test() {
            A::foo();
            parent::foo();
            self::foo();
        }

        public static function who() {
            echo __CLASS__."\n";
        }
    }
    class C extends B {
        public static function who() {
            echo __CLASS__."\n";
        }
    }

    C::test();
    ?&gt;
</pre>

<small>
    A<br>
    C<br>
    C
</small>

</body>
</html>
