<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/constants.php">Назад</a></li>
</ul>

<h1>Синтаксис</h1>

<p>Константа может быть определена с помощью ключевого слова const или с помощью функции define(). В то время как define() позволяет задать константу через выражение, конструкция const ограничена. После того, как константа определена, её значение не может быть изменено или аннулировано.</p>

<p>При использовании ключевого слова const допускаются только скалярные выражения (bool, int, float и string) и константы array, содержащие только скалярные выражения. Можно определить константы с типом resource, но не рекомендуется, так как это может привести к неожиданным результатам.</p>

<p>Получить значение константы можно, указав её имя. В отличие от переменных, вам не нужно предварять имя константы символом $. Также можно использовать функцию constant() для получения значения константы, если вы формируете имя константы динамически. Используйте функцию get_defined_constants() для получения списка всех определённых констант.</p>

<p><b>Замечание:</b> Константы и (глобальные) переменные находятся в разных пространствах имён. Это означает, что, например, true и $TRUE в целом отличаются.</p>

<p>Если используется неопределённая константа, выбрасывается Error. До PHP 8.0.0 неопределённые константы интерпретировались как простое слово string, то есть (CONSTANT vs "CONSTANT"). Этот резервный вариант объявлен устаревшим с PHP 7.2.0, при этом будет сгенерирована ошибка уровня E_WARNING. До PHP 7.2.0 вместо этого выдавалась ошибка уровня E_NOTICE. Смотрите также главу руководства, которая разъясняет, почему $foo[bar] - это неправильно (если bar не является константой). Это не относится к (полностью) определённым константам, которые всегда будут выбрасывать Error, если они не определены.</p>

<p><b>Замечание:</b> Чтобы проверить, установлена ли константа, используйте функцию defined().</p>

<figure>
    <figcaption>Различия между константами и переменными:</figcaption>
    <ul>
        <li>У констант нет приставки в виде знака доллара ($);</li>
        <li>Константы могут быть определены и доступны в любом месте без учёта области видимости;</li>
        <li>Константы не могут быть переопределены или удалены после первоначального объявления; и</li>
        <li>Константы могут иметь только скалярные значения или массивы.</li>
    </ul>
</figure>

<p><b>Замечание:</b> В отличие от определения констант с помощью функции define(), константы, объявленные с помощью ключевого слова const должны быть объявлены в самой верхней области видимости, потому что они определяются при компилировании скрипта. Это означает, что их нельзя объявлять внутри функций, циклов, выражений if и блоков try/ catch.</p>

</body>
</html>
