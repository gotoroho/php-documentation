<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/references.php">Назад</a></li>
</ul>

<h1>Что делают ссылки</h1>

<p>Есть три основных операции с использованием ссылок: присвоение по ссылке, передача по ссылке и возврат по ссылке. Данный раздел познакомит вас с этими операциями и предоставит ссылки для дальнейшего изучения.</p>

<h2>Присвоение по ссылке</h2>

<p>Первая из них - ссылки PHP позволяют создать две переменные указывающие на одно и то же значение. Таким образом, когда выполняется <code>$a =& $b</code>, то $a указывает на то же значение что и $b.</p>

<p><b>Замечание:</b> $a и $b здесь абсолютно эквивалентны, но это не означает, что $a указывает на $b или наоборот. Это означает, что $a и $b указывают на одно и то же значение.</p>

<p><b>Замечание:</b> При присвоении, передаче или возврате неинициализированной переменной по ссылке, происходит её создание.</p>

<pre>
    &lt;?php
    function foo(&$var) { }

    foo($a); // $a создана и равна null

    $b = array();
    foo($b['b']);
    var_dump(array_key_exists('b', $b)); // bool(true)

    $c = new StdClass;
    foo($c->d);
    var_dump(property_exists($c, 'd')); // bool(true)
    ?&gt;
</pre>

<p>Такой же синтаксис может использоваться в функциях, возвращающими ссылки, и с оператором new:</p>

<pre>
    &lt;?php
    $foo =& find_var($bar);
    ?&gt;
</pre>

<p>Использование того же синтаксиса с функцией, котораяне возвращает по ссылке, приведёт к ошибке, так же как и её использование с результатом оператора new. Хотя объекты передаются как указатели, это не то же самое, что ссылки, как описано в разделе Объекты и ссылки.</p>

<p><b>Внимание</b> Если переменной, объявленной внутри функции как global, будет присвоена ссылка, она будет видна только в функции. Чтобы избежать этого, используйте массив $GLOBALS.</p>

<p><b>Замечание:</b> При использовании переменной-ссылки в foreach, изменяется содержание, на которое она ссылается.</p>

<pre>
    &lt;?php
    $ref = 0;
    $row =& $ref;
    foreach (array(1, 2, 3) as $row) {
        // сделать что-нибудь
    }
    echo $ref; // 3 - последнее значение, используемое в цикле
    ?&gt;
</pre>

<p>Хотя в выражениях, создаваемых с помощью конструкции array(), нет явного присвоения по ссылке, тем не менее они могут вести себя как таковые, если указать префикс & для элементов массива.</p>

<p>Однако следует отметить, что ссылки в массивах являются потенциально опасными. При обычном (не по ссылке) присвоении массива, ссылки внутри этого массива сохраняются. Это также относится и к вызовам функций, когда массив передаётся по значению.</p>

<pre>
    &lt;?php
    /* Присвоение скалярных переменных */
    $a = 1;
    $b =& $a;
    $c = $b;
    $c = 7; //$c не ссылка и не изменяет значений $a и $b

    /* Присвоение массивов */
    $arr = array(1);
    $a =& $arr[0]; // $a и $arr[0] ссылаются на одно значение
    $arr2 = $arr; // присвоение не по ссылке!
    $arr2[0]++;
    /* $a == 2, $arr == array(2) */
    /* Содержимое $arr изменилось, хотя было присвоено не по ссылке! */
    ?&gt;
</pre>

<p>Иными словами, поведение отдельных элементов массива не зависит от типа присвоения этого массива.</p>

<h2>Передача по ссылке</h2>

<p>Второе, что делают ссылки - передача параметров по ссылке. При этом локальная переменная в функции и переменная в вызывающей области видимости ссылаются на одно и то же содержимое.</p>

<pre>
    &lt;?php
    function foo(&$var) {
        $var++;
    }

    $a = 5;
    foo($a);
    ?&gt;
</pre>

<p>Этот код присвоит $a значение 6. Это происходит, потому что в функции foo переменная $var ссылается на то же содержимое, что и переменная $a. Смотрите также детальное объяснение передачи по ссылке.</p>

<h2>Возврат по ссылке</h2>

<p>Третье, что могут делать ссылки - это возврат по ссылке.</p>

</body>
</html>
