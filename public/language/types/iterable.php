<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/types.php">Назад</a></li>
</ul>

<h1>Итерируемые</h1>

<p>Массив или объект, реализующий интерфейс Traversable. Можно проходиться по ним в цикле. По умолчанию может быть null или [].</p>

<pre>
    &lt;?php
    function foo(iterable $iterable = []) {
        // ...
    }

    function bar(): iterable {
        return [1, 2, 3];
    }

    function gen(): iterable {
        yield 1;
        yield 2;
        yield 3;
    }
    ?&gt;
</pre>

</body>
</html>
