<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/attributes.php">Назад</a></li>
</ul>

<h1>Синтаксис атрибутов</h1>

<p>Синтаксис атрибутов состоит из нескольких частей. Во-первых, декларация атрибута всегда начинается с символа #[ и заканчивается ]. Внутри перечисление из одного или более, разделённых запятой, атрибутов. Атрибуты можно задавать с помощью неполных, полных и абсолютных имён, как описано в разделе Использование пространства имён: основы. Аргументы атрибутов опциональны, но если они есть, то заключаются в скобки (). Аргументы атрибутов могут быть либо конкретными значениями, либо константными выражениями. Для аргументов можно использовать как позиционный синтаксис, так и синтаксис именованных аргументов.</p>

<p>Когда атрибут запрашивается с помощью Reflection API, его имя трактуется как имя класса, а аргументы передаются в его конструктор. Таким образом, для каждого атрибута должен существовать соответствующий класс.</p>

<pre>
    &lt;?php
    // a.php
    namespace MyExample;

    use Attribute;

    #[Attribute]
    class MyAttribute
    {
        const VALUE = 'value';

        private $value;

        public function __construct($value = null)
        {
            $this->value = $value;
        }
    }

    // b.php

    namespace Another;

    use MyExample\MyAttribute;

    #[MyAttribute]
    #[\MyExample\MyAttribute]
    #[MyAttribute(1234)]
    #[MyAttribute(value: 1234)]
    #[MyAttribute(MyAttribute::VALUE)]
    #[MyAttribute(array("key" => "value"))]
    #[MyAttribute(100 + 200)]
    class Thing
    {
    }

    #[MyAttribute(1234), MyAttribute(5678)]
    class AnotherThing
    {
    }
</pre>

</body>
</html>
