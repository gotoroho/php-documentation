<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Константы</h1>

<ul>
    <li><a href="/language/constants/basics.php">Основы</a></li>
    <li><a href="/language/constants/syntax.php">Синтаксис</a></li>
    <li><a href="/language/constants/predefined.php">Предопределённые константы</a></li>
    <li><a href="/language/constants/magic.php">Магические константы</a></li>
</ul>

</body>
</html>
