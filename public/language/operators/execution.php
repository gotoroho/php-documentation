<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/operators.php">Назад</a></li>
</ul>

<h1>Операторы исполнения</h1>

<p>PHP поддерживает один оператор исполнения: обратные кавычки (``). PHP попытается выполнить строку, заключённую в обратные кавычки, как консольную команду, и вернёт полученный вывод (то есть он не просто выводится на экран, а, например, может быть присвоен переменной). Использование обратных кавычек аналогично использованию функции shell_exec().</p>

<p><b>Замечание:</b> Обратные кавычки недоступны, в случае, если отключена функция shell_exec().</p>

<p><b>Замечание:</b> В отличие от некоторых других языков, обратные кавычки не будут работать внутри строк в двойных кавычках.</p>

<pre>
    # для понимания вывода консоли
    >_: echo -e test'\n'test
    >_: test
    >_: test

    &lt;?php
    echo `echo -e foo'\n'bar1'\n'bar2 | grep bar`;
    ?&gt;
</pre>

<small>
<?php
echo `echo -e foo'\n'bar1'\n'bar2 | grep bar`;
?>
</small>

</body>
</html>
