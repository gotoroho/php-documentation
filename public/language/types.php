<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Типы</h1>

<ul>
    <li><a href="/language/types/intro.php">Введение</a></li>
    <li><a href="/language/types/bool.php">bool</a></li>
    <li><a href="/language/types/int.php">int</a></li>
    <li><a href="/language/types/float.php">float</a></li>
    <li><a href="/language/types/string.php">string</a></li>
    <li><a href="/language/types/numeric-strings.php">Числовые строки</a></li>
    <li><a href="/language/types/array.php">Массивы</a></li>
    <li><a href="/language/types/iterable.php">Итерируемые</a></li>
    <li><a href="/language/types/object.php">Объекты</a></li>
    <li><a href="/language/types/resource.php">Ресурс</a></li>
    <li><a href="/language/types/null.php">Null</a></li>
    <li><a href="/language/types/callable.php">Callable</a></li>
    <li><a href="/language/types/declarations.php">Объявление типов</a></li>
    <li><a href="/language/types/type-juggling.php">Манипуляции с типами</a></li>
</ul>

</body>
</html>
