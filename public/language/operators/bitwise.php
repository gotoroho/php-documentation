<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/operators.php">Назад</a></li>
</ul>

<h1>Побитовые операторы</h1>

<p>Побитовые операторы позволяют считывать и устанавливать конкретные биты целых чисел.</p>

<table>
    <caption><strong>Побитовые операторы</strong></caption>

    <thead>
    <tr>
        <th>Пример</th>
        <th>Название</th>
        <th>Результат</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>$a & $b</td>
        <td>И</td>
        <td>Устанавливаются только те биты, которые установлены и в $a, и в $b.</td>
    </tr>
    <tr>
        <td>$a | $b</td>
        <td>Или</td>
        <td>Устанавливаются те биты, которые установлены в $a или в $b.</td>
    </tr>
    <tr>
        <td>$a ^ $b</td>
        <td>Исключающее или</td>
        <td>Устанавливаются только те биты, которые установлены либо только в $a, либо только в $b, но не в обоих одновременно.</td>
    </tr>
    <tr>
        <td>~ $a</td>
        <td>Отрицание</td>
        <td>Устанавливаются те биты, которые не установлены в $a, и наоборот.</td>
    </tr>
    <tr>
        <td>$a << $b</td>
        <td>Сдвиг влево</td>
        <td>Все биты переменной $a сдвигаются на $b позиций влево (каждая позиция подразумевает "умножение на 2")</td>
    </tr>
    <tr>
        <td>$a >> $b</td>
        <td>Сдвиг вправо</td>
        <td>Все биты переменной $a сдвигаются на $b позиций вправо (каждая позиция подразумевает "деление на 2")</td>
    </tr>
    </tbody>
</table>

<p>Побитовый сдвиг в PHP - это арифметическая операция. Биты, сдвинутые за границы числа, отбрасываются. Сдвиг влево дополняет число нулями справа, сдвигая в то же время знаковый бит числа влево, что означает что знак операнда не сохраняется. Сдвиг вправо сохраняет копию сдвинутого знакового бита слева, что означает что знак операнда сохраняется.</p>

<p>Используйте скобки для обеспечения необходимого приоритета операторов. Например, $a & $b == true сначала проверяет на равенство, а потом выполняет побитовое и; тогда как ($a & $b) == true сначала выполняет побитовое и, а потом выполняет проверку на равенство.</p>

<p>Если оба операнда для &, | и ^ строки, то операция будет производиться с кодами ASCII всех символов строки и в результате вернёт строку. Во всех остальных случаях, оба операнда будут преобразованы к целому и результатом будет целое число.</p>

<p>Если операнд для ~ строка, то операция будет производиться с кодами ASCII всех символов строки и в результате вернёт строку, в ином случае как операнд, так и результат, будут считаться целыми.</p>

<p>Оба операнда и результат выполнения << и >> всегда считаются за целое.</p>

<p><b>Внимание</b> Используйте функции из расширения gmp для побитовых операций над числами, большими чем PHP_INT_MAX.</p>

</body>
</html>
