<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Трейты</h1>

<p>Трейт - это механизм обеспечения повторного использования кода в языках с поддержкой только одиночного наследования, таких как PHP. Трейт предназначен для уменьшения некоторых ограничений одиночного наследования, позволяя разработчику повторно использовать наборы методов свободно, в нескольких независимых классах и реализованных с использованием разных архитектур построения классов. Семантика комбинации трейтов и классов определена таким образом, чтобы снизить уровень сложности, а также избежать типичных проблем, связанных с множественным наследованием и смешиванием (mixins).</p>

<h2>insteadof && as</h2>

<pre>
    &lt;?php
    trait A {
        public function smallTalk() {
            echo 'a';
        }
        public function bigTalk() {
            echo 'A';
        }
    }

    trait B {
        public function smallTalk() {
            echo 'b';
        }
        public function bigTalk() {
            echo 'B';
        }
    }

    class Talker {
        use A, B {
            B::smallTalk insteadof A;
            A::bigTalk insteadof B;
        }
    }

    class Aliased_Talker {
        use A, B {
            B::smallTalk insteadof A;
            A::bigTalk insteadof B;
            B::bigTalk as talk;
        }
    }
    ?&gt;
</pre>

<h2>Изменение видимости метода</h2>

<pre>
    &lt;?php
    trait HelloWorld {
        public function sayHello() {
            echo 'Hello World!';
        }
    }

    // Изменение видимости метода sayHello
    class MyClass1 {
        use HelloWorld { sayHello as protected; }
    }

    // Создание псевдонима метода с изменённой видимостью
    // видимость sayHello не изменилась
    class MyClass2 {
        use HelloWorld { sayHello as private myPrivateHello; }
    }
    ?&gt;
</pre>

<h2>Трейты, состоящие из трейтов</h2>

<pre>
    &lt;?php
    trait Hello {
        public function sayHello() {
            echo 'Hello ';
        }
    }

    trait World {
        public function sayWorld() {
            echo 'World!';
        }
    }

    trait HelloWorld {
        use Hello, World;
    }

    class MyHelloWorld {
        use HelloWorld;
    }

    $o = new MyHelloWorld();
    $o->sayHello();
    $o->sayWorld();
    ?&gt;
</pre>

<pre>
    &lt;?php
    trait PropertiesTrait {
        public $same = true;
        public $different = false;
    }

    class PropertiesExample {
        use PropertiesTrait;
        public $same = true;
        public $different = true; // Фатальная ошибка
    }
    ?&gt;
</pre>

</body>
</html>
