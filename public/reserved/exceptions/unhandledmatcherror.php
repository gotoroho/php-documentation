<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/exceptions.php">Назад</a></li>
</ul>

<h1>UnhandledMatchError</h1>

<p>UnhandledMatchError выбрасывается, если субъект, переданный в выражение match, не обрабатывается ни одной из сторон выражения match.</p>

</body>
</html>
