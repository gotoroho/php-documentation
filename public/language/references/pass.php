<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/references.php">Назад</a></li>
</ul>

<h1>Передача по ссылке</h1>

<p>Вы можете передать переменную в функцию по ссылке, чтобы она могла изменять значение аргумента. Синтаксис выглядит следующим образом:</p>

<pre>
    &lt;?php
    function foo(&$var) {
        $var++;
    }

    $a = 5;
    foo($a);
    // $a здесь равно 6
    ?&gt;
</pre>

<p><b>Замечание:</b> В вызове функции отсутствует знак ссылки - он есть только в определении функции. Этого достаточно для корректной передачи аргументов по ссылке.</p>

<figure>
    <figcaption>По ссылке можно передавать:</figcaption>

    <ul>
        <li>Переменные, например foo($a)</li>
        <li>Ссылки, возвращаемые функцией, например:</li>
    </ul>
</figure>

<pre>
    &lt;?php
    function foo(&$var) {
        $var++;
    }
    function &bar() {
        $a = 5;
        return $a;
    }
    foo(bar());
    ?&gt;
</pre>

<p>Любое другое выражение не должно передаваться по ссылке, так как результат не определён. Например, следующая передача по ссылке является неправильной:</p>

<pre>
    &lt;?php
    function foo(&$var) {
        $var++;
    }
    function bar() { // Операция & отсутствует
        $a = 5;
        return $a;
    }
    foo(bar()); // Вызывает предупреждение

    foo($a = 5); // Выражение, а не переменная
    foo(5); // Константа, а не переменная

    class Foobar
    {
    }

    foo(new Foobar()) // Вызывает уведомление с PHP 7.0.7
    // Notice: Only variables should be passed by reference
    ?&gt;
</pre>

</body>
</html>
