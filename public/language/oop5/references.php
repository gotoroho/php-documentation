<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Объекты и ссылки</h1>

<p>Одним из ключевых моментов объектно-ориентированной парадигмы PHP, который часто обсуждается, является "передача объектов по ссылке по умолчанию". Это не совсем верно. Данный раздел уточняет это понятие, используя некоторые примеры.</p>

<p>Ссылка в PHP - это псевдоним (алиас), который позволяет присвоить двум переменным одинаковое значение. В PHP объектная переменная больше не содержит сам объект как значение. Такая переменная содержит только идентификатор объекта, который позволяет найти конкретный объект при обращении к нему. Когда объект передаётся как аргумент функции, возвращается или присваивается другой переменной, то эти разные переменные не являются псевдонимами (алиасами): они содержат копию идентификатора, который указывает на один и тот же объект.</p>

</body>
</html>
