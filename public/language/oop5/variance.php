<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Ковариантность и контравариантность</h1>

<p>В PHP 7.2.0 была добавлена частичная контравариантность путём устранения ограничений типа для параметров в дочернем методе. Начиная с PHP 7.4.0, добавлена полная поддержка ковариантности и контравариантности.</p>

<p>Ковариантность позволяет дочернему методу возвращать более конкретный тип, чем тип возвращаемого значения его родительского метода. В то время как контравариантность позволяет типу параметра в дочернем методе быть менее специфичным, чем в родительском.</p>

<figure>
    <figcaption>Объявление типа считается более конкретным в следующем случае:</figcaption>

    <ul>
        <li>Удалён тип union</li>
        <li>Тип <i>возвращаемого?</i> класса изменяется на тип дочернего класса</li>
        <li>Число с плавающей точкой (float) изменено на целое число (int)</li>
        <li>iterable изменён на массив (array) или Traversable</li>
    </ul>
</figure>

<p>В противном случае класс типа считается менее конкретным.</p>

<h2>Ковариантность</h2>

<pre>
    &lt;?php
    interface AnimalShelter
    {
        public function adopt(string $name): Animal;
    }

    class CatShelter implements AnimalShelter
    {
        public function adopt(string $name): Cat // Возвращаем класс Cat вместо Animal
        {
            return new Cat($name);
        }
    }
    ?&gt;
</pre>

<h2>Контравариантность</h2>

<pre>
    &lt;?php

    class Food {}

    class AnimalFood extends Food {}

    abstract class Animal
    {
        protected string $name;

        public function __construct(string $name)
        {
            $this->name = $name;
        }

        public function eat(AnimalFood $food)
        {
            echo $this->name . " ест " . get_class($food);
        }
    }

    class Dog extends Animal
    {
        public function eat(Food $food) {
            echo $this->name . " ест " . get_class($food);
        }
    }
    ?&gt;
</pre>

</body>
</html>
