<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/interfaces.php">Назад</a></li>
</ul>

<h1>Класс WeakReference</h1>

<p>Класс WeakReference предоставляет способ доступа к объекту, не влияя при этом на количество ссылок на него, таким образом сборщик мусора сможет освободить этот объект.</p>

<p>Объект класса WeakReference не может быть сериализован.</p>

<ul>
    <li>WeakReference::__construct — Конструктор, который запрещает реализацию</li>
    <li>WeakReference::create — Создаёт новую слабую ссылку</li>
    <li>WeakReference::get — Получает объект со слабой ссылкой</li>
</ul>

</body>
</html>
