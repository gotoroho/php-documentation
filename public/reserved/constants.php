<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="javascript:" onclick="history.back()">Назад</a></li>
</ul>

<h1>Предопределённые константы</h1>

<h2>Объявленные в ядре константы</h2>

<p>Эти константы объявляются ядром PHP и охватывают PHP, Zend engine и SAPI-модули.</p>

<dl>
    <dt>PHP_VERSION (string)</dt>
    <dd>Текущая версия PHP в виде строки в формате "major.minor.release[extra]".</dd>

    <dt>PHP_MAJOR_VERSION (int)</dt>
    <dd>Текущая "основная" (major) версия PHP в виде целого числа (например, int(5) для версии "5.2.7-extra").</dd>

    <dt>PHP_MINOR_VERSION (int)</dt>
    <dd>Текущая "промежуточная" (minor) версия PHP в виде целого числа (например, int(2) для версии "5.2.7-extra").</dd>

    <dt>PHP_RELEASE_VERSION (int)</dt>
    <dd>Текущая "релиз"-версия (release) PHP в виде целого числа (например, int(7) для версии "5.2.7-extra").</dd>

    <dt>PHP_VERSION_ID (int)</dt>
    <dd>Текущая версия PHP в виде целого числа, её удобно использовать при сравнениях версий (например, int(50207) для версии "5.2.7-extra").</dd>

    <dt>PHP_EXTRA_VERSION (string)</dt>
    <dd>Текущая "экстра"-версия PHP в виде строки (например, '-extra' для версии "5.2.7-extra"). Обычно используется в различных дистрибутивах для индикации версий пакетов.</dd>

    <dt>PHP_ZTS (int)</dt>
    <dt>PHP_DEBUG (int)</dt>

    <dt>PHP_MAXPATHLEN (int)</dt>
    <dd>Максимальная длина файловых имён (включая путь), поддерживаемая данной сборкой PHP.</dd>

    <dt>PHP_OS (string)</dt>
    <dd>Операционная система, под которую собирался PHP.</dd>

    <dt>PHP_OS_FAMILY (string)</dt>
    <dd>Семейство операционных систем, для которых собран PHP. Любая из 'Windows', 'BSD', 'Darwin', 'Solaris', 'Linux' или 'unknown'. Доступно с PHP 7.2.0.</dd>

    <dt>PHP_SAPI (string)</dt>
    <dd>API сервера (Server API) данной сборки PHP. Смотрите также php_sapi_name().</dd>

    <dt>PHP_EOL (string)</dt>
    <dd>Корректный символ конца строки, используемый на данной платформе.</dd>

    <dt>PHP_INT_MAX (int)</dt>
    <dd>Максимальное целое число, поддерживаемое данной сборкой PHP. Обычно это int(2147483647) в 32-битных системах и int(9223372036854775807) в 64-битных. Обычно, PHP_INT_MIN === ~PHP_INT_MAX.</dd>

    <dt>PHP_INT_MIN (int)</dt>
    <dd>Минимальное целое число, поддерживаемое данной сборкой PHP. Обычно это int(-2147483648) в 32-битных системах и int(-9223372036854775808) в 64-битных.</dd>

    <dt>PHP_INT_SIZE (int)</dt>
    <dd>Размер целого числа в байтах в текущей сборке PHP.</dd>

    <dt>PHP_FLOAT_DIG (int)</dt>
    <dd>Количество десятичных цифр, которые могут быть округлены в float и обратно без потери точности. Доступно с PHP 7.2.0.</dd>

    <dt>PHP_FLOAT_EPSILON (float)</dt>
    <dd>Наименьшее положительное число x, такое, что x + 1.0 != 1.0. Доступно с PHP 7.2.0.</dd>

    <dt>PHP_FLOAT_MIN (float)</dt>
    <dd>Наименьшее возможное положительное число типа float. Если вам нужно наименьшее возможное отрицательное число типа float, используйте - PHP_FLOAT_MAX. Доступно с PHP 7.2.0.</dd>

    <dt>PHP_FLOAT_MAX (float)</dt>
    <dd>Максимальное возможное число типа float. Доступно с PHP 7.2.0.</dd>

    <dt>DEFAULT_INCLUDE_PATH (string)</dt>
    <dt>PEAR_INSTALL_DIR (string)</dt>
    <dt>PEAR_EXTENSION_DIR (string)</dt>

    <dt>PHP_EXTENSION_DIR (string)</dt>
    <dd>Каталог по умолчанию, в котором следует искать динамически загружаемые модули (если он не переопределён в extension_dir). По умолчанию используетсяPHP_PREFIX (или PHP_PREFIX . "\\ext" в Windows).</dd>

    <dt>PHP_PREFIX (string)</dt>
    <dd>Значение --prefix было установлено при настройке. В Windows это значение --with-prefix было установлено при настройке.</dd>

    <dt>PHP_BINDIR (string)</dt>
    <dd>Значение --bindir было установлено при настройке. В Windows это значение --with-prefix было установлено при настройке.</dd>

    <dt>PHP_BINARY (string)</dt>
    <dd>Указывает путь к исполняемым файлам PHP во время выполнения скрипта.</dd>

    <dt>PHP_MANDIR (string)</dt>
    <dd>Указывает путь установки страниц документации man.</dd>

    <dt>PHP_LIBDIR (string)</dt>
    <dt>PHP_DATADIR (string)</dt>
    <dt>PHP_SYSCONFDIR (string)</dt>
    <dt>PHP_LOCALSTATEDIR (string)</dt>
    <dt>PHP_CONFIG_FILE_PATH (string)</dt>
    <dt>PHP_CONFIG_FILE_SCAN_DIR (string)</dt>

    <dt>PHP_SHLIB_SUFFIX (string)</dt>
    <dd>Суффикс, используемый для динамически линкуемых библиотек, таких как "so" (для большинства Unix-систем) или "dll" (Windows).</dd>

    <dt>PHP_FD_SETSIZE (string)</dt>
    <dd>Максимальное количество файловых дескрипторов для системных вызовов. Доступно с PHP 7.1.0.</dd>

    <dt>E_ERROR (int)</dt>
    <dd>Константа, указывающая уровень сообщений об ошибках</dd>

    <dt>E_WARNING (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_PARSE (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_NOTICE (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_CORE_ERROR (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_CORE_WARNING (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_COMPILE_ERROR (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_COMPILE_WARNING (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_USER_ERROR (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_USER_WARNING (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_USER_NOTICE (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_RECOVERABLE_ERROR (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_DEPRECATED (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_USER_DEPRECATED (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_ALL (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>E_STRICT (int)</dt>
    <dd>Константа сообщения об ошибке</dd>

    <dt>__COMPILER_HALT_OFFSET__ (int)</dt>

    <dt>true (bool)</dt>
    <dd>Смотрите раздел Булев тип.</dd>

    <dt>false (bool)</dt>
    <dd>Смотрите раздел Булев тип.</dd>

    <dt>null (null)</dt>
    <dd>Смотрите Null.</dd>

    <dt>PHP_WINDOWS_EVENT_CTRL_C (int)</dt>
    <dd>Событие Windows CTRL+C. Доступно с PHP 7.4.0 (Только для Windows).</dd>

    <dt>PHP_WINDOWS_EVENT_CTRL_BREAK (int)</dt>
    <dd>Событие Windows CTRL+BREAK. Доступно с PHP 7.4.0 (Только для Windows).</dd>
</dl>

<p>Смотрите также: Магические константы.</p>

</body>
</html>
