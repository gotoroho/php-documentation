<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/variables.php">Назад</a></li>
</ul>

<h1>Суперглобальные переменные</h1>

<p>Суперглобальные переменные - это встроенные переменные, которые всегда доступны во всех областях видимости</p>

<p>Некоторые предопределённые переменные в PHP являются "суперглобальными", что означает, что они доступны в любом месте скрипта. Нет необходимости использовать синтаксис global $variable; для доступа к ним в функциях и методах.</p>

<p>Суперглобальными переменными являются:</p>

<ul>
    <li>$GLOBALS</li>
    <li>$_SERVER</li>
    <li>$_GET</li>
    <li>$_POST</li>
    <li>$_FILES</li>
    <li>$_COOKIE</li>
    <li>$_SESSION</li>
    <li>$_REQUEST</li>
    <li>$_ENV</li>
</ul>

<p><b>Замечание:</b> По умолчанию все суперглобальные переменные доступны всегда, однако существуют настройки, которые могут на это влиять. За дальнейшей информацией обращайтесь к описанию директивы variables_order.</p>

<p><b>Замечание:</b> Суперглобальные переменные не могут быть использованы в качестве переменных переменных внутри функций и методов.</p>

</body>
</html>
