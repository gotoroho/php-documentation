<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/control-structures.php">Назад</a></li>
</ul>

<h1>break</h1>

<p>break прерывает выполнение текущей структуры for, foreach, while, do-while или switch.</p>

<p>break принимает необязательный числовой аргумент, который сообщает ему выполнение какого количества вложенных структур необходимо прервать. Значение по умолчанию 1, только ближайшая структура будет прервана.</p>

<pre>
    &lt;?php
    $arr = array('один', 'два', 'три', 'четыре', 'стоп', 'пять');
    foreach ($arr as $val) {
        if ($val == 'стоп') {
            break;    /* Тут можно было написать 'break 1;'. */
        }
        echo "$val&lt;br /&gt;\n";
    }

    /* Использование дополнительного аргумента. */

    $i = 0;
    while (++$i) {
        switch ($i) {
            case 5:
                echo "Итерация 5&lt;br /&gt;\n";
                break 1;  /* Выйти только из конструкции switch. */
            case 10:
                echo "Итерация 10; выходим&lt;br /&gt;\n";
                break 2;  /* Выходим из конструкции switch и из цикла while. */
            default:
                break;
        }
    }
    ?&gt;
</pre>

<p>You can also use break with parentheses: break(1);</p>

<p><b>Note:</b> Using more nesting level leads to fatal error.</p>

</body>
</html>
