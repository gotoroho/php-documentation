<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/control-structures.php">Назад</a></li>
</ul>

<h1>goto</h1>

<img src="/images/goto.png" alt="Mem about goto">

<p>Оператор goto используется для перехода в другую часть программы. Место, куда необходимо перейти указывается с помощью чувствительный к регистру метки, за которой ставится двоеточие, после оператора goto указывается желаемая метка для перехода. Оператор не является неограниченным "goto". Целевая метка должна находиться в том же файле, в том же контексте. Имеется в виду, что вы не можете ни перейти за границы функции или метода, ни перейти внутрь одной из них. Вы также не можете перейти внутрь любой циклической структуры или оператора switch. Но вы можете выйти из них, и обычным применением оператора goto является использование его вместо многоуровневых break.</p>

<pre>
    &lt;?php
    for($i=0,$j=50; $i<100; $i++) {
        while($j--) {
            if($j==17) goto end;
        }
    }
    echo "i = $i";
    end:
    echo 'j hit 17';
    ?&gt;
</pre>

<small>
    j hit 17
</small>



</body>
</html>
