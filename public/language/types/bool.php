<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/types.php">Назад</a></li>
</ul>

<h1>bool</h1>

<pre>
    &lt;?php
    var_dump((bool) "");        // bool(false)
    var_dump((bool) 1);         // bool(true)
    var_dump((bool) -2);        // bool(true)
    var_dump((bool) "foo");     // bool(true)
    var_dump((bool) 2.3e5);     // bool(true)
    var_dump((bool) array(12)); // bool(true)
    var_dump((bool) array());   // bool(false)
    var_dump((bool) "false");   // bool(true)
    ?&gt;
</pre>

<h2>При преобразовании в bool, следующие значения рассматриваются как false:</h2>
<ul>
    <li>само значение boolean false</li>
    <li>integer 0 (ноль)</li>
    <li>float 0.0 (ноль) и -0.0 (минус ноль)</li>
    <li>пустая строка, и строка "0"</li>
    <li>массив без элементов</li>
    <li>особый тип NULL (включая неустановленные переменные)</li>
    <li>Объекты SimpleXML, созданные из пустых элементов без атрибутов, то есть элементов, не имеющих ни дочерних элементов, ни атрибутов.</li>
</ul>

</body>
</html>
