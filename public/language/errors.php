<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Ошибки</h1>

<ul>
    <li><a href="/language/errors/basics.php">Основы</a></li>
    <li><a href="/language/errors/php7.php">Ошибки в PHP 7</a></li>
</ul>

<p>К сожалению, как бы мы ни были аккуратны при написании кода, ошибки являются частью нашей жизни. PHP будет сообщать об ошибках, предупреждениях и уведомлениях о самых распространённых проблемах кода и времени исполнения, но знания, как разобраться в ошибках и как их обработать, могут сильно упростить процесс отладки.</p>

</body>
</html>
