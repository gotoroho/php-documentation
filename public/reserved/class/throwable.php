<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/interfaces.php">Назад</a></li>
</ul>

<h1>Throwable</h1>

<p>Throwable является родительским интерфейсом для всех объектов, выбрасывающихся с помощью выражения throw, включая классы Error и Exception.</p>

<p><b>Замечание:</b> Классы PHP не могут напрямую реализовать интерфейс Throwable. Вместо этого они могут наследовать подкласс Exception.</p>

<ul>
    <li>Throwable::getMessage — Получает сообщение ошибки</li>
    <li>Throwable::getCode — Возвращает код исключения</li>
    <li>Throwable::getFile — Возвращает файл, в котором произошло исключение</li>
    <li>Throwable::getLine — Получает строку скрипта, в которой данный объект был выброшен</li>
    <li>Throwable::getTrace — Возвращает трассировку стека</li>
    <li>Throwable::getTraceAsString — Получает результаты трассировки стека в виде строки</li>
    <li>Throwable::getPrevious — Возвращает предыдущий Throwable</li>
    <li>Throwable::__toString — Получает строковое представление выброшенного объекта</li>
</ul>

</body>
</html>
