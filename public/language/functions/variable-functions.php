<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/functions.php">Назад</a></li>
</ul>

<h1>Обращение к функциям через переменные</h1>

<p>PHP поддерживает концепцию переменных функций. Это означает, что если к имени переменной присоединены круглые скобки, PHP ищет функцию с тем же именем, что и результат вычисления переменной, и пытается её выполнить. Эту возможность можно использовать для реализации обратных вызовов, таблиц функций и множества других вещей.</p>

<p>Переменные функции не будут работать с такими языковыми конструкциями как echo, print, unset(), isset(), empty(), include, require и т.п. Вам необходимо реализовать свою функцию-обёртку для того, чтобы приведённые выше конструкции могли работать с переменными функциями.</p>

<pre>
    &lt;?php
    function foo() {
        echo "В foo()&lt;br /&gt;\n";
    }

    function bar($arg = '')
    {
        echo "В bar(); аргумент был '$arg'.&lt;br /&gt;\n";
    }

    // Функция-обёртка для echo
    function echoit($string)
    {
        echo $string;
    }

    $func = 'foo';
    $func();        // Вызывает функцию foo()

    $func = 'bar';
    $func('test');  // Вызывает функцию bar()

    $func = 'echoit';
    $func('test');  // Вызывает функцию echoit()
    ?&gt;
</pre>

<p>Вы также можете вызвать методы объекта используя возможности PHP для работы с переменными функциями.</p>

<p>При вызове статических методов вызов функции "сильнее", чем оператор доступа к статическому свойству.</p>

<pre>
    &lt;?php
    class Foo
    {
        static $variable = 'статическое свойство';
        static function Variable()
        {
            echo 'Вызов метода Variable';
        }
    }

    echo Foo::$variable; // Это выведет 'статическое свойство'. Переменная $variable будет разрешена в нужной области видимости.
    $variable = "Variable";
    Foo::$variable();  // Это вызовет $foo->Variable(), прочитав $variable из этой области видимости.

    ?&gt;
</pre>

</body>
</html>
