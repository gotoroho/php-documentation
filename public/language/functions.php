<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Функции</h1>

<ul>
    <li><a href="/language/functions/user-defined.php">Функции, определяемые пользователем</a></li>
    <li><a href="/language/functions/arguments.php">Аргументы функции</a></li>
    <li><a href="/language/functions/returning-values.php">Возврат значений</a></li>
    <li><a href="/language/functions/variable-functions.php">Обращение к функциям через переменные</a></li>
    <li><a href="/language/functions/internal.php">Встроенные функции</a></li>
    <li><a href="/language/functions/anonymous.php">Анонимные функции</a></li>
    <li><a href="/language/functions/arrow.php">Стрелочные функции</a></li>
</ul>

</body>
</html>
