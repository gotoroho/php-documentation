<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/interfaces.php">Назад</a></li>
</ul>

<h1>Интерфейс Stringable</h1>

<small>php 8</small>

<p>Интерфейс Stringable обозначает класс, реализующий метод __toString(). В отличие от большинства интерфейсов, Stringable неявно присутствует в любом классе, в котором определён магический метод __toString(), хотя он может и должен быть объявлен явно.</p>

<p>Его основное значение - разрешить функциям выполнять проверку типа на соответствие типу union string|Stringable, чтобы принимать либо строковый примитив, либо объект, который может быть преобразован в строку.</p>

</body>
</html>
