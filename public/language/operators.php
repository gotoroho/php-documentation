<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Операторы</h1>

<ul>
    <li><a href="/language/operators/basics.php">Основы</a></li>
    <li><a href="/language/operators/precedence.php">Приоритет оператора</a></li>
    <li><a href="/language/operators/arithmetic.php">Арифметические операторы</a></li>
    <li><a href="/language/operators/assignment.php">Оператор присваивания</a></li>
    <li><a href="/language/operators/bitwise.php">Побитовые операторы</a></li>
    <li><a href="/language/operators/comparison.php">Операторы сравнения</a></li>
    <li><a href="/language/operators/error-control.php">Оператор управления ошибками</a></li>
    <li><a href="/language/operators/execution.php">Операторы исполнения</a></li>
    <li><a href="/language/operators/increment.php">Операторы инкремента и декремента</a></li>
    <li><a href="/language/operators/logical.php">Логические операторы</a></li>
    <li><a href="/language/operators/string.php">Строковые операторы</a></li>
    <li><a href="/language/operators/array.php">Операторы, работающие с массивами</a></li>
    <li><a href="/language/operators/type.php">Оператор проверки типа</a></li>
</ul>

</body>
</html>
