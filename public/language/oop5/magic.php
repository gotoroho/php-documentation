<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Магические методы</h1>

<p><b>Предостережение</b> Все имена методов, начинающиеся с __, зарезервированы PHP. Не рекомендуется использовать имена методов с __ в PHP, если вы не хотите использовать соответствующую магическую функциональность.</p>

<ul>
    <li>__construct()</li>
    <li>__destruct()</li>
    <li>__call()</li>
    <li>__callStatic()</li>
    <li>__get()</li>
    <li>__set()</li>
    <li>__isset()</li>
    <li>__unset()</li>
    <li>__sleep()</li>
    <li>__wakeup()</li>
    <li>__serialize()</li>
    <li>__unserialize()</li>
    <li>__toString()</li>
    <li>__invoke()</li>
    <li>__set_state()</li>
    <li>__clone()</li>
    <li>__debugInfo()</li>
</ul>

<p><b>Внимание </b>Все магические методы, за исключением __construct(), __destruct() и __clone(), ДОЛЖНЫ быть объявлены как public, в противном случае будет вызвана ошибка уровня E_WARNING. До PHP 8.0.0 для магических методов __sleep(), __wakeup(), __serialize(), __unserialize() и __set_state() не выполнялась проверка.</p>

<p><b>Внимание </b>Если объявления типа используются в определении магического метода, они должны быть идентичны сигнатуре, описанной в этом документе. В противном случае выдаётся фатальная ошибка. До PHP 8.0.0 диагностические сообщения не отправлялись.</p>

<h2>__sleep() и __wakeup()</h2>

<p>Функция serialize() проверяет, присутствует ли в классе метод с магическим именем __sleep(). Если это так, то этот метод выполняется до любой операции сериализации. Он может очистить объект и должен возвращать массив с именами всех переменных этого объекта, которые должны быть сериализованы. Если метод ничего не возвращает, то сериализуется null и выдаётся предупреждение E_NOTICE.</p>

<p><b>Замечание:</b> Недопустимо возвращать в __sleep() имена закрытых свойств в родительском классе. Это приведёт к ошибке уровня E_NOTICE. Вместо этого вы можете использовать __serialize().</p>

<h2>__serialize() и __unserialize()</h2>

<p><b>Замечание:</b> Если и __serialize() и __sleep() определены в одном и том же объекте, будет вызван только метод __serialize(). __sleep() будет игнорироваться. Если объект реализует интерфейс Serializable, метод serialize() интерфейса будет игнорироваться, а вместо него будет использован __serialize().</p>

<p>Предполагаемое использование __serialize() заключается в определении удобного для сериализации произвольного представления объекта. Элементы массива могут соответствовать свойствам объекта, но это не обязательно.</p>

<p>И наоборот, unserialize() проверяет наличие магической функции __unserialize(). Если функция присутствует, ей будет передан восстановленный массив, который был возвращён из __serialize(). Затем он может восстановить свойства объекта из этого массива соответствующим образом.</p>

<p><b>Замечание:</b> Если и __unserialize() и __wakeup() определены в одном и том же объекте, будет вызван только метод __unserialize(). __wakeup() будет игнорироваться.</p>

<p><b>Замечание:</b> Функция доступна с PHP 7.4.0.</p>

<pre>
    &lt;?php
    class Connection
    {
        protected $link;
        private $dsn, $username, $password;

        public function __construct($dsn, $username, $password)
        {
            $this->dsn = $dsn;
            $this->username = $username;
            $this->password = $password;
            $this->connect();
        }

        private function connect()
        {
            $this->link = new PDO($this->dsn, $this->username, $this->password);
        }

        public function __serialize(): array
        {
            return [
                'dsn' => $this->dsn,
                'user' => $this->username,
                'pass' => $this->password,
            ];
        }

        public function __unserialize(array $data): void
        {
            $this->dsn = $data['dsn'];
            $this->username = $data['user'];
            $this->password = $data['pass'];

            $this->connect();
        }
    }
    ?&gt;
</pre>

<h2>__toString()</h2>

<article>
    <b>Внимание</b>

    <p>Начиная с PHP 8.0.0, возвращаемое значение следует стандартной семантике типа PHP, что означает, что оно будет преобразовано в строку (string), если возможно, и если strict typing отключён.</p>

    <p>Начиная с PHP 8.0.0, любой класс, содержащий метод __toString(), также будет неявно реализовывать интерфейс Stringable и, таким образом, будет проходить проверку типа для этого интерфейса В любом случае рекомендуется явно реализовать интерфейс.</p>

    <p>В PHP 7.4 возвращаемое значение ДОЛЖНО быть строкой (string), иначе выдаётся Error.</p>

    <p>До PHP 7.4.0 возвращаемое значение должно быть строкой (string), в противном случае выдаётся фатальная ошибка E_RECOVERABLE_ERROR. is emitted.</p>
</article>

<p><b>Внимание</b> Нельзя выбросить исключение из метода __toString() до PHP 7.4.0. Это приведёт к фатальной ошибке.</p>

<h2>__invoke()</h2>

<p>Метод __invoke() вызывается, когда скрипт пытается выполнить объект как функцию.</p>

<h2>__set_state()</h2>

<p>Этот статический метод вызывается для тех классов, которые экспортируются функцией var_export().</p>

<p>Единственным параметром этого метода является массив, содержащий экспортируемые свойства в виде ['property' => value, ...].</p>

<p><b>Замечание:</b> При экспорте объекта var_export() не проверяет, реализует ли класс объекта метод __set_state(), поэтому повторный импорт таких объектов не будет выполнен, если метод __set_state() не реализован. В частности, это относится к некоторым внутренним классам. Необходимость проверки, реализует ли импортируемый класс метод __set_state(), полностью лежит на разработчике.</p>

<h2>__debugInfo()</h2>

<p>Этот метод вызывается функцией var_dump(), когда необходимо вывести список свойств объекта. Если этот метод не определён, тогда будут выведены все свойства объекта c модификаторами public, protected и private.</p>

</body>
</html>
