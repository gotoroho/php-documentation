<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/errors.php">Назад</a></li>
</ul>

<h1>Ошибки в PHP 7</h1>

<p>В PHP 7 механизм сообщения об ошибках был сильно изменён. Традиционное оповещение об ошибке в PHP 5 было заменено новым механизмом, в котором большинство ошибок вызываются с помощью исключений класса Error.</p>

<p>Как и обычные исключения, исключения Error вызываются до появления первого соответствующего блока catch. Если соответствующие блоки не предусмотрены, то будет вызван любой обработчик исключений, установленный с помощью set_exception_handler(). В случае отсутствия обработчика по умолчанию, исключение будет конвертировано в фатальную ошибку и будет обработано как традиционная ошибка.</p>

<p>Поскольку класс Error не наследуется от класса Exception, блок catch (Exception $e) { ... } для обработки неперехваченных исключений PHP 5 не может перехватить исключения Error. Для их перехвата используйте блок catch (Error $e) { ... } или установите обработчик исключений с помощью set_exception_handler().</p>

<h2>Иерархия Error</h2>

<ul>
    <li>
        <span><a href="https://www.php.net/manual/class.throwable.php">Throwable</a></span>
        <ul>
            <li>
                <span><a href="https://www.php.net/manual/class.error.php">Error</a></span>
                <ul>
                    <li>
                        <span><a href="https://www.php.net/manual/class.arithmeticerror.php">ArithmeticError</a></span>
                        <ul>
                            <li>
                                <span><a href="https://www.php.net/manual/class.divisionbyzeroerror.php">DivisionByZeroError</a></span>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span><a href="https://www.php.net/manual/class.assertionerror.php">AssertionError</a></span>
                    </li>
                    <li>
                        <span><a href="https://www.php.net/manual/class.compileerror.php">CompileError</a></span>
                        <ul>
                            <li>
                                <span><a href="https://www.php.net/manual/class.parseerror.php">ParseError</a></span>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span><a href="https://www.php.net/manual/class.typeerror.php">TypeError</a></span>
                        <ul>
                            <li>
                                <span><a href="https://www.php.net/manual/class.argumentcounterror.php">ArgumentCountError</a></span>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span><a href="https://www.php.net/manual/class.valueerror.php">ValueError</a></span>
                    </li>
                    <li>
                        <span><a href="https://www.php.net/manual/class.unhandledmatcherror.php">UnhandledMatchError</a></span>
                    </li>
                </ul>
            </li>
            <li>
                <span><a href="https://www.php.net/manual/class.exception.php">Exception</a></span>
                <ul>
                    <li>
                        <span>...</span>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>

</body>
</html>
