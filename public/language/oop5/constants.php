<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Константы классов</h1>

<p>Константы также могут быть объявлены в пределах одного класса. Область видимости констант по умолчанию public.</p>

<p>Замечание: Константы класса могут быть переопределены дочерним классом.</p>

<p>Интерфейсы также могут содержать константы. За примерами обращайтесь к разделу об интерфейсах.</p>

<p>К классу можно обратиться с помощью переменной. Значение переменной не может быть ключевым словом (например, self, parent и static).</p>

<p>Обратите внимание, что константы класса задаются один раз для всего класса, а не отдельно для каждого созданного объекта этого класса.</p>

<pre>
    &lt;?php
    class MyClass
    {
        const CONSTANT = 'значение константы';

        function showConstant() {
            echo  self::CONSTANT . "\n";
        }
    }

    echo MyClass::CONSTANT . "\n";

    $classname = "MyClass";
    echo $classname::CONSTANT . "\n";

    $class = new MyClass();
    $class->showConstant();

    echo $class::CONSTANT."\n";
    ?&gt;
</pre>

<p>Специальная константа ::class, которой на этапе компиляции присваивается полное имя класса, полезна при использовании с классами, использующими пространства имён.</p>

<p><b>Замечание:</b> Начиная с PHP 7.1.0 для констант класса можно использовать модификаторы области видимости.</p>

</body>
</html>
