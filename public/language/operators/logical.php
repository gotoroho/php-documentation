<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/operators.php">Назад</a></li>
</ul>

<h1>Логические операторы</h1>

<table>
    <caption><strong>Логические операторы</strong></caption>

    <thead>
    <tr>
        <th>Пример</th>
        <th>Название</th>
        <th>Результат</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>$a and $b</td>
        <td>И</td>
        <td>true, если и $a, и $b true.</td>
    </tr>
    <tr>
        <td>$a or $b</td>
        <td>Или</td>
        <td>true, если или $a, или $b true.</td>
    </tr>
    <tr>
        <td>$a xor $b</td>
        <td>Исключающее или</td>
        <td>true, если $a, или $b true, но не оба.</td>
    </tr>
    <tr>
        <td>! $a</td>
        <td>Отрицание</td>
        <td>true, если $a не true.</td>
    </tr>
    <tr>
        <td>$a && $b</td>
        <td>И</td>
        <td>true, если и $a, и $b true.</td>
    </tr>
    <tr>
        <td>$a || $b</td>
        <td>Или</td>
        <td>true, если или $a, или $b true.</td>
    </tr>
    </tbody>
</table>

</body>
</html>
