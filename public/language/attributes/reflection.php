<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/attributes.php">Назад</a></li>
</ul>

<h1>Чтение атрибутов с помощью Reflection API</h1>

<p>Для доступа к атрибутам классов, методов, функций, параметров, свойств и констант класса, в Reflection API присутствует метод getAttributes() для каждого из перечисленных объектов рефлексии. Этот метод возвращает массив экземпляров ReflectionAttribute, у каждого из которых можно запросить имя атрибута и его аргументы, а также инстанцирование объекта, представляющего атрибут.</p>

<p>Такое отделение свойств атрибута от явного инстанцирования даёт программисту более полный контроль над обработкой ошибок, связанных с отсутствующим классом атрибута и некорректностью его аргументов. Объект атрибута будет создан и проверен на корректность аргументов только после вызова ReflectionAttribute::newInstance(), не раньше.</p>

<pre>
    &lt;?php

    #[Attribute]
    class MyAttribute
    {
        public $value;

        public function __construct($value)
        {
            $this->value = $value;
        }
    }

    #[MyAttribute(value: 1234)]
    class Thing
    {
    }

    function dumpAttributeData($reflection) {
        $attributes = $reflection->getAttributes();

        foreach ($attributes as $attribute) {
           var_dump($attribute->getName());
           var_dump($attribute->getArguments());
           var_dump($attribute->newInstance());
        }
    }

    dumpAttributeData(new ReflectionClass(Thing::class));
    /*
    string(11) "MyAttribute"
    array(1) {
      ["value"]=>
      int(1234)
    }
    object(MyAttribute)#3 (1) {
      ["value"]=>
      int(1234)
    }
    */
</pre>

<p>Вместо того, чтобы последовательно перебирать все атрибуты объекта рефлексии, можно указать имя класса в качестве аргумента и получить только подходящие атрибуты.</p>

</body>
</html>
