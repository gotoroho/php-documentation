<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/interfaces.php">Назад</a></li>
</ul>

<h1>Интерфейс Iterator</h1>

<p>Интерфейс для внешних итераторов или объектов, которые могут повторять себя изнутри.</p>

<h2>Предопределённые итераторы</h2>

<p>PHP уже предоставляет некоторые итераторы для многих ежедневных задач. Смотрите список итераторов SPL для более детальной информации.</p>

<pre>
    &lt;?php
    class myIterator implements Iterator {
        private $position = 0;
        private $array = array(
            "firstelement",
            "secondelement",
            "lastelement",
        );

        public function __construct() {
            $this->position = 0;
        }

        public function rewind() {
            var_dump(__METHOD__);
            $this->position = 0;
        }

        public function current() {
            var_dump(__METHOD__);
            return $this->array[$this->position];
        }

        public function key() {
            var_dump(__METHOD__);
            return $this->position;
        }

        public function next() {
            var_dump(__METHOD__);
            ++$this->position;
        }

        public function valid() {
            var_dump(__METHOD__);
            return isset($this->array[$this->position]);
        }
    }

    $it = new myIterator;

    foreach($it as $key => $value) {
        var_dump($key, $value);
        echo "\n";
    }
    ?&gt;

    string(18) "myIterator::rewind"
    string(17) "myIterator::valid"
    string(19) "myIterator::current"
    string(15) "myIterator::key"
    int(0)
    string(12) "firstelement"

    string(16) "myIterator::next"
    string(17) "myIterator::valid"
    string(19) "myIterator::current"
    string(15) "myIterator::key"
    int(1)
    string(13) "secondelement"

    string(16) "myIterator::next"
    string(17) "myIterator::valid"
    string(19) "myIterator::current"
    string(15) "myIterator::key"
    int(2)
    string(11) "lastelement"

    string(16) "myIterator::next"
    string(17) "myIterator::valid"
</pre>

</body>
</html>
