<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/namespaces.php">Назад</a></li>
</ul>

<h1>Определение пространств имён</h1>

<p>Хотя любой корректный PHP-код может находиться внутри пространства имён, только классы (включая абстрактные и трейты), интерфейсы, функции и константы зависят от него.</p>

<p>Пространства имён объявляются с помощью зарезервированного слова namespace. Файл, содержащий пространство имён, должен содержать его объявление в начале перед любым другим кодом, кроме зарезервированного слова declare.</p>

<p><b>Замечание:</b> Абсолютные имена (т.е. имена, начинающиеся с обратной косой черты) не допускаются в объявлениях пространства имён, поскольку такие конструкции интерпретируются как относительные выражения пространства имён.</p>

<p>Только выражение declare может находиться перед объявлением пространства имён для указания кодировки файла. Кроме того, объявлению пространства имён не должен предшествовать не PHP-код, в том числе лишние пробелы:</p>

<p>Так же как файлы и каталоги, пространства имён PHP позволяют создавать иерархию имён. Таким образом, имя пространства может быть определено с подуровнями.</p>

</body>
</html>
