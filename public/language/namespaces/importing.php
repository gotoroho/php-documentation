<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/namespaces.php">Назад</a></li>
</ul>

<h1>Использование пространств имён: импорт/создание псевдонима имени</h1>

<p>Возможность ссылаться на внешнее абсолютное имя по псевдониму или импортированию - это важная особенность пространств имён. Это похоже на возможность файловых систем unix создавать символические ссылки на файл или директорию.</p>

<p>PHP может создавать псевдонимы имени/импортировать константы, функции, классы, интерфейсы и пространства имён.</p>

<p>Создание псевдонима имени выполняется с помощью оператора use. Вот пример, показывающий 5 типов импорта:</p>

<pre>
    &lt;?php
    namespace foo;
    use My\Full\Classname as Another;

    // это тоже самое, что и использование My\Full\NSname as NSname
    use My\Full\NSname;

    // импортирование глобального класса
    use ArrayObject;

    // импортирование функции
    use function My\Full\functionName;

    // псевдоним функции
    use function My\Full\functionName as func;

    // импортирование константы
    use const My\Full\CONSTANT;

    $obj = new namespace\Another; // создаёт экземпляр класса foo\Another
    $obj = new Another; // создаёт объект класса My\Full\Classname
    NSname\subns\func(); // вызывает функцию My\Full\NSname\subns\func
    $a = new ArrayObject(array(1)); // создаёт объект класса ArrayObject
    // без выражения "use ArrayObject" мы создадим объект класса foo\ArrayObject
    func(); // вызывает функцию My\Full\functionName
    echo CONSTANT; // выводит содержимое константы My\Full\CONSTANT
    ?&gt;
</pre>

<p>Обратите внимание, что для имён в пространстве имён (абсолютные имена, содержащие разделитель пространств имён, такие как Foo\Bar, в отличие от глобальных имён, которые его не содержат, такие как FooBar) нет необходимости в начальном обратном слеше (\) и его присутствие там не рекомендуется, так как импортируемые имена должны быть абсолютными и не обрабатываются относительно текущего пространства имён.</p>

<p>PHP дополнительно поддерживает удобное сокращение для задания нескольких операторов use в одной и той же строке</p>

<pre>
    &lt;?php
    use My\Full\Classname as Another, My\Full\NSname;

    $obj = new Another; // создаёт объект класса My\Full\Classname
    NSname\subns\func(); // вызывает функцию My\Full\NSname\subns\func
    ?&gt;
</pre>

<p>В дополнение, импорт распространяется только на неполные и полные имена. Абсолютные имена не затрагиваются операцией импорта.</p>

<pre>
    &lt;?php
    use My\Full\Classname as Another, My\Full\NSname;

    $obj = new Another; // создаёт объект класса My\Full\Classname
    $obj = new \Another; // создаёт объект класса Another
    $obj = new Another\thing; // создаёт объект класса My\Full\Classname\thing
    $obj = new \Another\thing; // создаёт объект класса Another\thing
    ?&gt;
</pre>

<h2>Обзор правил для импорта</h2>

<p>Ключевое слово use должно быть указано в самом начале файла (в глобальной области) или внутри объявления пространства имён. Это необходимо потому, что импорт выполняется во время компиляции, а не во время исполнения, поэтому оно не может быть заключено в блок. Следующий пример показывает недопустимое применение ключевого слова use:</p>

<pre>
    &lt;?php
    namespace Languages;

    function toGreenlandic()
    {
        use Languages\Danish; // PHP Parse error:  syntax error, unexpected 'use' (T_USE)

        //...
    }
    ?&gt;
</pre>

<p><b>Замечание:</b> Правила импорта задаются на каждый файл отдельно. Это означает, что присоединяемые файлы НЕ будут наследовать правила импорта из родительского файла.</p>

<h2>Описание группирования в одном операторе use</h2>

<pre>
    &lt;?php

    use some\namespace\ClassA;
    use some\namespace\ClassB;
    use some\namespace\ClassC as C;

    use function some\namespace\fn_a;
    use function some\namespace\fn_b;
    use function some\namespace\fn_c;

    use const some\namespace\ConstA;
    use const some\namespace\ConstB;
    use const some\namespace\ConstC;

    // Эквивалентно следующему групповому использованию
    use some\namespace\{ClassA, ClassB, ClassC as C};
    use function some\namespace\{fn_a, fn_b, fn_c};
    use const some\namespace\{ConstA, ConstB, ConstC};

    ?&gt;
</pre>

</body>
</html>
