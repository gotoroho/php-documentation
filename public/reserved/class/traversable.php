<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/interfaces.php">Назад</a></li>
</ul>

<h1>Интерфейс Traversable</h1>

<p>Интерфейс, определяющий, является ли класс обходимым (traversable) с использованием foreach.</p>

<p>Абстрактный базовый интерфейс, который не может быть реализован сам по себе. Вместо этого должен реализовываться IteratorAggregate или Iterator.</p>

<p><b>Замечание:</b> Внутренние (встроенные) классы, которые реализуют этот интерфейс, могут быть использованы в конструкции foreach и не обязаны реализовывать IteratorAggregate или Iterator.</p>

<p><b>Замечание:</b> Это внутренний интерфейс, который не может быть реализован в скрипте PHP. Вместо него нужно использовать либо IteratorAggregate, либо Iterator. При реализации интерфейса, наследующего от Traversable, убедитесь, что в секции implements перед его именем стоит IteratorAggregate или Iterator.</p>

</body>
</html>
