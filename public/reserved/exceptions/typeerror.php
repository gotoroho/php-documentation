<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/exceptions.php">Назад</a></li>
</ul>

<h1>TypeError</h1>

<figure>
    <figcaption>Есть три сценария, в которых будет выброшено исключение TypeError:</figcaption>

    <ul>
        <li>Тип аргумента, переданного функции, не соответствует типу, объявленному в функции для этого аргумента.</li>
        <li>Тип возвращённого функцией значения не соответствует типу возврата, объявленному в функции.</li>
        <li>Встроенной функции PHP было передано неверное количество аргументов (только для режима strict).</li>
    </ul>
</figure>

</body>
</html>
