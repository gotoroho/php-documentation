<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Классы и объекты</h1>

<ul>
    <li><a href="/language/namespaces/rationale.php">Обзор пространств имён</a></li>
    <li><a href="/language/namespaces/definition.php">Определение пространств имён</a></li>
    <li><a href="/language/namespaces/definitionally.php">Описание нескольких пространств имён в одном файле</a></li>
    <li><a href="/language/namespaces/basics.php">Использование пространства имён: основы</a></li>
    <li><a href="/language/namespaces/dynamic.php">Пространства имён и динамические особенности языка</a></li>
    <li><a href="/language/namespaces/nsconstants.php">Ключевое слово namespace и константа __NAMESPACE__</a></li>
    <li><a href="/language/namespaces/importing.php">Использование пространств имён: импорт/создание псевдонима имени</a></li>
    <li><a href="/language/namespaces/global.php">Глобальное пространство</a></li>
    <li><a href="/language/namespaces/fallback.php">Использование пространств имён: переход к глобальной функции/константе</a></li>
    <li><a href="/language/namespaces/rules.php">Правила разрешения имён</a></li>
</ul>

</body>
</html>
