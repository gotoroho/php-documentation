<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/types.php">Назад</a></li>
</ul>

<h1>Введение</h1>

<h2>10 простых типов</h2>

<figure>
    <figcaption>4 скалярных:</figcaption>
    <ol>
        <li>bool</li>
        <li>int</li>
        <li>float (double)</li>
        <li>string</li>
    </ol>
</figure>

<figure>
    <figcaption>4 смешанных:</figcaption>
    <ol start="5">
        <li>array</li>
        <li>object</li>
        <li>callable</li>
        <li>iterable</li>
    </ol>
</figure>

<figure>
    <figcaption>2 специальных:</figcaption>
    <ol start="9">
        <li>resource</li>
        <li>NULL</li>
    </ol>
</figure>

<small>Пример:</small>
<pre><small>    &lt;?php
    $a_bool = TRUE;   // логический
    $a_str  = "foo";  // строковый
    $a_str2 = 'foo';  // строковый
    $an_int = 12;     // целочисленный

    echo gettype($a_bool); // выводит: boolean
    echo gettype($a_str);  // выводит: string

    // Если это целое, увеличить на четыре
    if (is_int($an_int)) {
        $an_int += 4;
    }

    // Если $a_bool - это строка, вывести её
    // (ничего не выводит)
    if (is_string($a_bool)) {
        echo "Строка: $a_bool";
    }
    ?&gt;
</small></pre>

<h2>Приведение и set type</h2>

<pre>
    &lt;?php
    // приведение $foo к типу boolean
    $foo = 10;   // $foo - это целое число
    $bar = (boolean) $foo;   // $bar - это булев тип
    ?&gt;

    &lt;?php
    // set type $foo boolean
    $foo = 10;   // $foo - это целое число
    $bar = settype($foo, "bool");   // $bar - это булев тип
    ?&gt;
</pre>

<small>Допустимые приведения типов:
    <br>(int), (integer)
    <br>(bool), (boolean)
    <br>(float), (double), (real)
    <br>(string)
    <br>(array)
    <br>(object)
    <br>(unset)
</small>
<br><br>
<small>Допустипые значения 2-го аргумента set type:
    <br>"boolean" или "bool"
    <br>"integer" или "int"
    <br>"float" или "double"
    <br>"string"
    <br>"array"
    <br>"object"
    <br>"null"
</small>

<h2>echo && var_dump</h2>

<h3>Код скрипта</h3>
<pre><?php
    $fileName = __DIR__ . "/examples/types_example.php";
    $file = fopen($fileName, "r");
    echo htmlentities(fread($file, filesize($fileName)));
    ?>
</pre>

<h3>Результат</h3>
<pre><?php
    require __DIR__ . "/examples/types_example.php";
    $example = new TypesExample();
    $example->main();
    ?>
</pre>

</body>
</html>
