<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Встроенные интерфейсы и классы</h1>

<ul>
    <li><a href="/reserved/class/traversable.php">Traversable</a></li>
    <li><a href="/reserved/class/iterator.php">Iterator</a></li>
    <li><a href="/reserved/class/iteratoraggregate.php">IteratorAggregate</a></li>
    <li><a href="/reserved/class/throwable.php">Throwable</a></li>
    <li><a href="/reserved/class/arrayaccess.php">ArrayAccess</a></li>
    <li><a href="/reserved/class/serializable.php">Serializable</a></li>
    <li><a href="/reserved/class/closure.php">Closure</a></li>
    <li><a href="/reserved/class/generator.php">Generator</a></li>
    <li><a href="/reserved/class/weakreference.php">WeakReference</a></li>
    <li><a href="/reserved/class/weakmap.php">WeakMap</a></li>
    <li><a href="/reserved/class/stringable.php">Stringable</a></li>
</ul>

</body>
</html>
