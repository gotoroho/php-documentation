<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/generators.php">Назад</a></li>
</ul>

<h1>Сравнение генераторов с объектами класса Iterator</h1>

<p>Главное преимущество генераторов - это их простота. Гораздо меньше шаблонного кода надо написать, по сравнению с реализацией объекта класса Iterator, и этот код гораздо более простой и понятный. К примеру, эта функция и класс делают одно и то же.</p>

<pre>
    &lt;?php
    function getLinesFromFile($fileName) {
        if (!$fileHandle = fopen($fileName, 'r')) {
            return;
        }

        while (false !== $line = fgets($fileHandle)) {
            yield $line;
        }

        fclose($fileHandle);
    }

    // Против...

    class LineIterator implements Iterator {
        protected $fileHandle;

        protected $line;
        protected $i;

        public function __construct($fileName) {
            if (!$this->fileHandle = fopen($fileName, 'r')) {
                throw new RuntimeException('Невозможно открыть файл "' . $fileName . '"');
            }
        }

        public function rewind() {
            fseek($this->fileHandle, 0);
            $this->line = fgets($this->fileHandle);
            $this->i = 0;
        }

        public function valid() {
            return false !== $this->line;
        }

        public function current() {
            return $this->line;
        }

        public function key() {
            return $this->i;
        }

        public function next() {
            if (false !== $this->line) {
                $this->line = fgets($this->fileHandle);
                $this->i++;
            }
        }

        public function __destruct() {
            fclose($this->fileHandle);
        }
    }
    ?&gt;
</pre>

<p>Однако за эту простоту, впрочем, приходится платить: генераторы могут быть только однонаправленными итераторами. Их нельзя перемотать назад после старта итерации. Это также означает, что один и тот же генератор нельзя использовать несколько раз: генератор необходимо пересоздавать каждый раз, снова вызвав функцию генератора.</p>

</body>
</html>
