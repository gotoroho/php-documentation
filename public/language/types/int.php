<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/types.php">Назад</a></li>
</ul>

<h1>int</h1>

<pre>
    &lt;?php
    $a = 1234; // десятичное число
    $a = 0123; // восьмеричное число (эквивалентно 83 в десятичной системе)
    $a = 0x1A; // шестнадцатеричное число (эквивалентно 26 в десятичной системе)
    $a = 0b11111111; // двоичное число (эквивалентно 255 в десятичной системе)
    $a = 1_234_567; // десятичное число (с PHP 7.4.0)
    ?&gt;
</pre>

<ul>
    <li><pre>
десятичные  : [1-9][0-9]*(_[0-9]+)*
            | 0</li>

    <li><pre>шестнадцатеричные : 0[xX][0-9a-fA-F]+(_[0-9a-fA-F]+)*</li>

    <li><pre>восьмеричные      : 0[0-7]+(_[0-7]+)*</li>

    <li><pre>двоичные          : 0[bB][01]+(_[01]+)*</li>

    <li><pre>
целые   : десятичные
        | шестнадцатеричные
        | восьмеричные
        | двоичные</li>
</ul>

<h2>Преобразование</h2>

<h3>Из булевого типа</h3>
<p>false преобразуется в 0 (ноль), а true - в 1 (единицу).</p>

<h3>Из чисел с плавающей точкой</h3>
<p>При преобразовании из float в int, число будет округлено в сторону нуля.</p>
<p>Если число с плавающей точкой превышает размеры int (обычно +/- 2.15e+9 = 2^31 на 32-битных системах и +/- 9.22e+18 = 2^63 на 64-битных системах, результат будет неопределённым, так как float не имеет достаточной точности, чтобы вернуть верный результат в виде целого числа (int). В этом случае не будет выведено ни предупреждения, ни даже замечания!</p>
<p><b>Замечание:</b> Значения NaN и Infinity при приведении к int становятся равными нулю, вместо неопределённого значения в зависимости от платформы.</p>
<p><b>Внимание</b> Никогда не приводите неизвестную дробь к int, так как это иногда может дать неожиданные результаты.</p>
<pre>
    &lt;?php
    echo (int) ( (0.1+0.7) * 10 ); // выводит 7!
    ?&gt;
</pre>

<h3>Из строк</h3>
<p>Если строка содержит числа или ведущая числовая, тогда она будет преобразована в соответствующее целочисленное значение, в противном случае она преобразуется в ноль (0).</p>

<h3>Из NULL</h3>
<p>Значение null всегда преобразуется в ноль (0).</p>
<p><b>Предостережение</b> Для других типов поведение преобразования в int не определено. Не полагайтесь на любое наблюдаемое поведение, так как оно может измениться без предупреждения.</p>

<small>
</small>

</body>
</html>
