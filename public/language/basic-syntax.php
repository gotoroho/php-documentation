<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<a href="/">К оглавлению</a>

<h1>Основы синтаксиса</h1>

<h2>Теги нормального человека</h2>

<ul>
    <li><code><?= "&lt;php echo \"Use in XHTML or XML\"; ?&gt;" ?></code></li>
    <li><code><?= "&lt;?= echo 'Short echo tag'; ?&gt;" ?></code></li>
</ul>

<h2>Теги больных битриксоидов (без обид)</h2>

<ul>
    <li><?= "&lt;? echo 'Enable short_open_tag'; ?&gt;" ?></li>
</ul>

<h2>Условие</h2>

<pre>
    &lt;?php if (time() % 2 === 0): ?&gt;
        Это будет отображено, если секунда чётная.
    &lt;?php else: ?&gt;
        В ином случае будет отображено это.
    &lt;?php endif; ?&gt;
</pre>

<small>Результат:
    <?php if (time() % 2 === 0): ?>
        Это будет отображено, если секунда чётная.
    <?php else: ?>
        В ином случае будет отображено это.
    <?php endif; ?>
</small>

<h2>New line feature</h2>

<pre>
    &lt;?php echo "Some text"; ?&gt;&lt;!-- Тут есть ; в конце линии - это обязательно почти всегда --&gt;
    No newline
    &lt;?= "But newline now" ?&gt;&lt;!-- Тут нет ; в конце линии - это нормально, закрывающий тег такое учитывает --&gt;
</pre>

<small>Результат:
    <?php echo "Some text"; ?>
    No newline
    <?= "But newline now" ?>
</small>

<br><br>

<b>* похоже, что не работает, если в самом коде нет nl</b>

<pre>&lt;?php echo "Some text"; ?&gt;No newline&lt;?= "But newline now" ?&gt;</pre>
<pre><?php echo "Some text"; ?>No newline<?= "But newline now" ?></pre>

<h2>Комментарии</h2>

<pre>
&lt;?php
    echo "Это тест"; // Это однострочный комментарий в стиле C++
    /* Это многострочный комментарий
        ещё одна строка комментария */
    echo "Это ещё один тест";
    echo "Последний тест"; # Это комментарий в стиле оболочки Unix
?&gt;

&lt;?php
    <span style="color: red;">/*</span>
        echo "Это тест"; /* Этот комментарий вызовет проблему <span style="color: red;">*/</span>
    */
?&gt;
</pre>

</body>
</html>
