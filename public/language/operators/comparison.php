<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/operators.php">Назад</a></li>
</ul>

<h1>Операторы сравнения</h1>

<p>Операторы сравнения, как это видно из их названия, позволяют сравнивать между собой два значения. Возможно вам будет интересно также ознакомиться с разделом Сравнение типов, в котором приведено большое количество соответствующих примеров.</p>

<table>
    <caption><strong>Операторы сравнения</strong></caption>

    <thead>
    <tr>
        <th>Пример</th>
        <th>Название</th>
        <th>Результат</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>$a == $b</td>
        <td>Равно</td>
        <td>true если $a равно $b после преобразования типов.</td>
    </tr>
    <tr>
        <td>$a === $b</td>
        <td>Тождественно равно</td>
        <td>true если $a равно $b и имеет тот же тип.</td>
    </tr>
    <tr>
        <td>$a != $b</td>
        <td>Не равно</td>
        <td>true если $a не равно $b после преобразования типов.</td>
    </tr>
    <tr>
        <td>$a <> $b</td>
        <td>Не равно</td>
        <td>true если $a не равно $b после преобразования типов.</td>
    </tr>
    <tr>
        <td>$a !== $b</td>
        <td>Тождественно не равно</td>
        <td>true если $a не равно $b, или они разных типов.</td>
    </tr>
    <tr>
        <td>$a < $b</td>
        <td>Меньше</td>
        <td>true если $a строго меньше $b.</td>
    </tr>
    <tr>
        <td>$a > $b</td>
        <td>Больше</td>
        <td>true если $a строго больше $b.</td>
    </tr>
    <tr>
        <td>$a <= $b</td>
        <td>Меньше или равно</td>
        <td>true если $a меньше или равно $b.</td>
    </tr>
    <tr>
        <td>$a >= $b</td>
        <td>Больше или равно</td>
        <td>true если $a больше или равно $b.</td>
    </tr>
    <tr>
        <td>$a <=> $b</td>
        <td>Космический корабль (spaceship)</td>
        <td>Число типа int меньше, больше или равное нулю, когда $a соответственно меньше, больше или равно $b.</td>
    </tr>
    </tbody>
</table>

<p>В случае, если оба операнда являются строками, содержащими числа или один операнд является числом, а другой - строкой, содержащей числа, то сравнение выполняется численно. Эти правила также применяются к оператору switch. Преобразование типа не происходит при сравнении === или !==, поскольку это включает сравнение типа, а также значения.</p>

<p><b>Внимание</b> До PHP 8.0.0, если строка (string) сравнивалась с числом или строкой, содержащей число, то строка (string) преобразовывалось в число перед выполнением сравнения. Это могло привести к неожиданным результатам</p>

<p>Для различных типов сравнение происходит в соответствии со следующей таблицей (по порядку).</p>

<table>
    <caption><strong>Сравнение различных типов</strong></caption>

    <thead>
    <tr>
        <th>Тип операнда 1</th>
        <th>Тип операнда 2</th>
        <th>Результат</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>null или string</td>
        <td>string</td>
        <td>null преобразуется в "", числовое или лексическое сравнение</td>
    </tr>
    <tr>
        <td>bool или null</td>
        <td>что угодно</td>
        <td>Преобразуется в тип bool, false < true</td>
    </tr>
    <tr>
        <td>object</td>
        <td>object</td>
        <td>Встроенные классы могут определять свои собственные правила сравнения, объекты разных классов не сравниваются, про сравнение объектов одного класса смотрите Сравнение объекта</td>
    </tr>
    <tr>
        <td>string, resource, int или float</td>
        <td>string, resource, int или float</td>
        <td>Строки и ресурсы переводятся в числа, обычная математика</td>
    </tr>
    <tr>
        <td>array</td>
        <td>array</td>
        <td>Массивы с меньшим числом элементов считаются меньше, если ключ из первого операнда не найден во втором операнде - массивы не могут сравниваться, иначе идёт сравнение соответствующих значений (смотрите пример ниже)</td>
    </tr>
    <tr>
        <td>array</td>
        <td>что угодно</td>
        <td>тип array всегда больше</td>
    </tr>
    <tr>
        <td>object</td>
        <td>что угодно</td>
        <td>тип object всегда больше</td>
    </tr>
    </tbody>
</table>

<p><b>Внимание</b> Сравнение чисел с плавающей точкой. Из-за особого внутреннего представления типа float, не нужно проверять на равенство два числа с плавающей точкой (float). Для более подробной информации смотрите документацию по типу float.</p>

<h2>Тернарный оператор</h2>

<p>Ещё одним условным оператором является тернарный оператор "?:".</p>

<p>Выражение (expr1) ? (expr2) : (expr3) интерпретируется как expr2, если expr1 имеет значение true, или как expr3, если expr1 имеет значение false.</p>

<p>Также стало возможным не писать среднюю часть тернарного оператора. Выражение expr1 ?: expr3 возвращает expr1 если expr1 имеет значение true и expr3 в противном случае.</p>

<p><b>Замечание:</b> Обратите внимание, что тернарный оператор является выражением и трактуется не как переменная, а как результат выражения. Это важно знать, если вы хотите вернуть переменную по ссылке. Выражение return $var == 42 ? $a : $b; не будет работать в функции, возвращающей значение по ссылке, а в более поздних версиях PHP также будет выдано предупреждение.</p>

<p><b>Замечание:</b> Рекомендуется избегать "нагромождения" тернарных выражений. Поведение PHP неочевидно при использовании более чем одного тернарного оператора без скобок в одном выражении неочевидно по сравнению с другими языками. Действительно, до PHP 8.0.0 троичные выражения оценивались лево-ассоциативными, а не право-ассоциативными, как в большинстве других языков программирования. Использование лево-ассоциативности устарело в PHP 7.4.0. Начиная с PHP 8.0.0, тернарный оператор неассоциативен.</p>

<h2>Оператор объединения с null</h2>

<p>Выражение (expr1) ?? (expr2) вычисляется так: expr2, если expr1 равен null и expr1 в противном случае. На практике, этот оператор не вызывает предупреждения или ошибки, если левый операнд не существует, как и isset(). Это очень полезно для ключей массива.</p>

<p><b>Замечание:</b> Пожалуйста помните, что этот оператор является выражением, и он приравнивается к выражению, а не значению переменной. Это может быть важным, если вы хотите вернуть значение по ссылке. Выражение return $foo ?? $bar; в функции возвращающей ссылку будет не работать, а выводить предупреждение.</p>

<p><b>Замечание:</b> Обратите внимание, что этот оператор позволяет использовать простую вложенность $a ?? $b ?? $c</p>

</body>
</html>
