<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/context.php">Назад</a></li>
</ul>

<h1>Параметры контекста FTP</h1>

<dl>
    <dt>overwrite bool</dt>
    <dd>Разрешает перезаписывать существующие файлы на удалённом сервере. Работает только в режиме записи (upload). По умолчанию false.</dd>

    <dt>resume_pos int</dt>
    <dd>Смещение в файле, с которого начинается передача. Работает только в режиме чтения (download). По умолчанию 0 (Начало файла).</dd>

    <dt>proxy string</dt>
    <dd>FTP-запрос через прокси-сервер HTTP. Применяется только при операции чтения файла. Пример: tcp://squid.example.com:8000.</dd>
</dl>

<p><b>Замечание:</b> Дополнительные опции контекста могут поддерживаться нижележащим транспортным протоколом. Для потоков ftp://, это относится к опциям контекста для транспортного протокола tcp://. Для потоков ftps://, это относится к опциям контекста для транспортного протокола ssl://.</p>

</body>
</html>
