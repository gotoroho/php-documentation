<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/interfaces.php">Назад</a></li>
</ul>

<h1>Интерфейс Serializable</h1>

<p>Интерфейс для индивидуальной сериализации.</p>

<p>Классы, реализующие этот интерфейс, больше не поддерживают __sleep() и __wakeup(). Метод serialize вызывается всякий раз, когда необходима сериализация экземпляру класса. Этот метод не вызывает __destruct() и не имеет никаких побочных действий кроме тех, которые запрограммированы внутри него. Когда данные десериализуются, класс известен и соответствующий метод unserialize() вызывается как конструктор вместо вызова __construct(). Если вам необходимо вызвать стандартный конструктор, вы можете это сделать в этом методе.</p>

<pre>
    interface Serializable {
        abstract public serialize(): ?string
        abstract public unserialize(string $data): void
    }
</pre>

</body>
</html>
