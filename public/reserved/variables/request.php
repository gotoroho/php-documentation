<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/variables.php">Назад</a></li>
</ul>

<h1>$_REQUEST</h1>

<p>Ассоциативный массив (array), который по умолчанию содержит данные переменных $_GET, $_POST и $_COOKIE.</p>

<p><b>Замечание:</b> При работе в командной строке переменные argv и argc не включаются в данный массив - они присутствуют в массиве $_SERVER.</p>

<p><b>Замечание:</b> Переменные в массиве $_REQUEST передаются в скрипт посредством методов GET, POST или COOKIE, поэтому им нельзя доверять, т.к. они могли быть изменены удалённым пользователем. Их наличие и порядок добавления данных в соответствующие массивы определяется директивой конфигурации PHP request_order и variables_order.</p>

</body>
</html>
