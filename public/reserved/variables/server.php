<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/variables.php">Назад</a></li>
</ul>

<h1>$_SERVER</h1>

<p>$_SERVER - Информация о сервере и среде исполнения</p>

<p>Переменная $_SERVER - это массив, содержащий информацию, такую как заголовки, пути и местоположения скриптов. Записи в этом массиве создаются веб-сервером. Нет гарантии, что каждый веб-сервер предоставит любую из них; сервер может опустить некоторые из них или предоставить другие, не указанные здесь. Тем не менее, многие эти переменные присутствуют в » спецификации CGI/1.1, так что вы можете ожидать их наличие.</p>

<dl>


    <dt>
        '<var class="varname">PHP_SELF</var>'</dt>

    <dd>

      <span class="simpara">
       Имя файла скрипта, который сейчас выполняется, относительно
       корня документов. Например, <var class="varname">$_SERVER['PHP_SELF']</var> в скрипте
       по адресу <var class="filename">http://example.com/foo/bar.php</var>
       будет <var class="filename">/foo/bar.php</var>.
       Константа <a href="language.constants.predefined.php" class="link">__FILE__</a>
       содержит полный путь и имя файла текущего (то есть
       подключённого) файла.
      </span>
        <span class="simpara">
       Если PHP запущен в командной строке, эта переменная содержит
       имя скрипта.
      </span>
    </dd>




    <dt>
        '<a href="reserved.variables.argv.php" class="link">argv</a>'</dt>

    <dd>

      <span class="simpara">
       Массив аргументов, переданных скрипту. Когда скрипт
       запущен в командой строке, это даёт C-подобный доступ
       к параметрам командной строки. Когда вызывается через метод GET,
       этот массив будет содержать строку запроса.
      </span>
    </dd>




    <dt>
        '<a href="reserved.variables.argc.php" class="link">argc</a>'</dt>

    <dd>

      <span class="simpara">
       Содержит количество параметров, переданных скрипту
       (если запуск произведён в командной строке).
      </span>
    </dd>




    <dt>
        '<var class="varname">GATEWAY_INTERFACE</var>'</dt>

    <dd>

      <span class="simpara">
       Содержит используемую сервером версию спецификации CGI;
       к примеру '<code class="literal">CGI/1.1</code>'.
      </span>
    </dd>




    <dt>
        '<var class="varname">SERVER_ADDR</var>'</dt>

    <dd>

      <span class="simpara">
       IP-адрес сервера, на котором выполняется текущий скрипт.
      </span>
    </dd>




    <dt>
        '<var class="varname">SERVER_NAME</var>'</dt>

    <dd>

      <span class="simpara">
       Имя хоста, на котором выполняется текущий скрипт.
       Если скрипт выполняется на виртуальном хосте,
       здесь будет содержатся имя, определённое для этого виртуального хоста.
      </span>
        <blockquote class="note"><p><strong class="note">Замечание</strong>:
                <span class="simpara">
        В Apache 2 вы должны установить <code class="literal">UseCanonicalName = On</code> и
        <code class="literal">ServerName</code>.
        В противном случае это значение отразит имя хоста, предоставленное
        клиентом, которое может быть подделано.
        Небезопасно полагаться на это значение в контексте, требующем безопасности.
       </span>
            </p></blockquote>
    </dd>




    <dt>
        '<var class="varname">SERVER_SOFTWARE</var>'</dt>

    <dd>

      <span class="simpara">
       Строка идентификации сервера, указанная в заголовках,
       когда происходит ответ на запрос.
      </span>
    </dd>




    <dt>
        '<var class="varname">SERVER_PROTOCOL</var>'</dt>

    <dd>

      <span class="simpara">
       Имя и версия информационного протокола, через который
       была запрошена страница; к примеру '<code class="literal">HTTP/1.0</code>';
      </span>
    </dd>




    <dt>
        '<var class="varname">REQUEST_METHOD</var>'</dt>

    <dd>

      <span class="simpara">
       Какой метод был использован для запроса страницы; к примеру '<code class="literal">GET</code>',
       '<code class="literal">HEAD</code>', '<code class="literal">POST</code>', '<code class="literal">PUT</code>'.
      </span>
        <blockquote class="note"><p><strong class="note">Замечание</strong>:
            </p><p class="para">
                PHP-скрипт завершается после отправки заголовков (то есть после того, как
                осуществляется любой вывод без буферизации вывода), если метод запроса был
                <code class="literal">HEAD</code>.
            </p>
        </blockquote>
    </dd>




    <dt>
        '<var class="varname">REQUEST_TIME</var>'</dt>

    <dd>

      <span class="simpara">
       Временная метка начала запроса.
      </span>
    </dd>




    <dt>
        '<var class="varname">REQUEST_TIME_FLOAT</var>'</dt>

    <dd>

      <span class="simpara">
       Временная метка начала запроса с точностью до микросекунд.
      </span>
    </dd>




    <dt>
        '<var class="varname">QUERY_STRING</var>'</dt>

    <dd>

      <span class="simpara">
       Строка запроса, если есть, через которую была открыта страница.
      </span>
    </dd>




    <dt>
        '<var class="varname">DOCUMENT_ROOT</var>'</dt>

    <dd>

      <span class="simpara">
       Директория корня документов, в которой выполняется текущий скрипт,
       в точности та, которая указана в конфигурационном файле сервера.
      </span>
    </dd>




    <dt>
        '<var class="varname">HTTP_ACCEPT</var>'</dt>

    <dd>

      <span class="simpara">
       Содержимое заголовка <code class="literal">Accept:</code> из текущего запроса,
       если он есть.
      </span>
    </dd>




    <dt>
        '<var class="varname">HTTP_ACCEPT_CHARSET</var>'</dt>

    <dd>

      <span class="simpara">
       Содержимое заголовка <code class="literal">Accept-Charset:</code> из
       текущего запроса, если он есть. Например:
       '<code class="literal">iso-8859-1,*,utf-8</code>'.
      </span>
    </dd>




    <dt>
        '<var class="varname">HTTP_ACCEPT_ENCODING</var>'</dt>

    <dd>

      <span class="simpara">
       Содержимое заголовка <code class="literal">Accept-Encoding:</code> из
       текущего запроса, если он есть. Например: '<code class="literal">gzip</code>'.
      </span>
    </dd>




    <dt>
        '<var class="varname">HTTP_ACCEPT_LANGUAGE</var>'</dt>

    <dd>

      <span class="simpara">
       Содержимое заголовка <code class="literal">Accept-Language:</code> из
       текущего запроса, если он есть. Например: '<code class="literal">en</code>'.
      </span>
    </dd>




    <dt>
        '<var class="varname">HTTP_CONNECTION</var>'</dt>

    <dd>

      <span class="simpara">
       Содержимое заголовка <code class="literal">Connection:</code> из
       текущего запроса, если он есть. Например: '<code class="literal">Keep-Alive</code>'.
      </span>
    </dd>




    <dt>
        '<var class="varname">HTTP_HOST</var>'</dt>

    <dd>

      <span class="simpara">
       Содержимое заголовка <code class="literal">Host:</code> из
       текущего запроса, если он есть.
      </span>
    </dd>




    <dt>
        '<var class="varname">HTTP_REFERER</var>'</dt>

    <dd>

      <span class="simpara">
       Адрес страницы (если есть), с которой браузер пользователя перешёл на эту страницу.
       Этот заголовок устанавливается браузером пользователя.
       Не все браузеры устанавливают его, а некоторые в качестве дополнительной
       возможности позволяют изменять содержимое заголовка
       <var class="varname">HTTP_REFERER</var>. Одним словом, нельзя доверять этому заголовку.
      </span>
    </dd>




    <dt>
        '<var class="varname">HTTP_USER_AGENT</var>'</dt>

    <dd>

      <span class="simpara">
       Содержимое заголовка <code class="literal">User-Agent:</code> из
       текущего запроса, если он есть. Эта строка, обозначающая браузер,
       который открыл данную страницу. Типичным
       примером является строка: <span class="computeroutput">Mozilla/4.5 [en] (X11; U;
       Linux 2.2.9 i586)</span>. Помимо всего прочего, вы можете
       использовать это значение с функцией <span class="function"><a href="function.get-browser.php" class="function">get_browser()</a></span>,
       чтобы адаптировать вывод вашей страницы к возможностям браузера пользователя.
      </span>
    </dd>




    <dt>
        '<var class="varname">HTTPS</var>'</dt>

    <dd>

      <span class="simpara">
       Принимает непустое значение, если запрос был произведён через протокол HTTPS.
      </span>
    </dd>




    <dt>
        '<var class="varname">REMOTE_ADDR</var>'</dt>

    <dd>

      <span class="simpara">
       IP-адрес, с которого пользователь просматривает текущую страницу.
      </span>
    </dd>




    <dt>
        '<var class="varname">REMOTE_HOST</var>'</dt>

    <dd>

      <span class="simpara">
       Удалённый хост, с которого пользователь просматривает текущую
       страницу. Обратный поиск DNS основан на значении переменной
       <var class="varname">REMOTE_ADDR</var>.
      </span>
        <blockquote class="note"><p><strong class="note">Замечание</strong>:
                <span class="simpara">
        Ваш сервер должен быть настроен, чтобы создавать эту переменную.
        Для примера, в Apache вам необходимо присутствие директивы
        <code class="literal">HostnameLookups On</code> в файле <var class="filename">httpd.conf</var>, чтобы эта
        переменная создавалась. Смотрите также <span class="function"><a href="function.gethostbyaddr.php" class="function">gethostbyaddr()</a></span>.
       </span>
            </p></blockquote>
    </dd>




    <dt>
        '<var class="varname">REMOTE_PORT</var>'</dt>

    <dd>

      <span class="simpara">
       Порт на удалённой машине, который используется для связи с сервером.
      </span>
    </dd>




    <dt>
        '<var class="varname">REMOTE_USER</var>'</dt>

    <dd>

      <span class="simpara">
       Аутентифицированный пользователь.
      </span>
    </dd>




    <dt>
        '<var class="varname">REDIRECT_REMOTE_USER</var>'</dt>

    <dd>

      <span class="simpara">
       Аутентифицированный пользователь, если запрос был перенаправлен изнутри.
      </span>
    </dd>




    <dt>
        '<var class="varname">SCRIPT_FILENAME</var>'</dt>

    <dd>

        <p class="para">
            Абсолютный путь к исполняемому скрипту.
        </p><blockquote class="note"><p><strong class="note">Замечание</strong>:
            </p><p class="para">
                Если скрипт запускается в командной строке (CLI), используя
                относительный путь, такой как <var class="filename">file.php</var> или
                <var class="filename">../file.php</var>, переменная
                <var class="varname">$_SERVER['SCRIPT_FILENAME']</var> будет
                содержать относительный путь, указанный пользователем.
            </p>
        </blockquote>

    </dd>




    <dt>
        '<var class="varname">SERVER_ADMIN</var>'</dt>

    <dd>

      <span class="simpara">
       Эта переменная получает своё значение (для Apache) из директивы
       конфигурационного файла сервера. Если скрипт запущен на
       виртуальном хосте, это будет значение,
       определённое для данного виртуального хоста.
      </span>
    </dd>




    <dt>
        '<var class="varname">SERVER_PORT</var>'</dt>

    <dd>

      <span class="simpara">
       Порт на компьютере сервера, используемый сервером для соединения.
       Для установок по умолчанию, значение будет '<code class="literal">80</code>';
       используя SSL, например, это значение будет таким, какое сконфигурировано
       для соединений безопасного HTTP.
      </span>
        <blockquote class="note"><p><strong class="note">Замечание</strong>:
                <span class="simpara">
        Чтобы получить физический (реальный) порт в Apache 2, необходимо
        установить <code class="literal">UseCanonicalName = On</code> и
        <code class="literal">UseCanonicalPhysicalPort = On</code>, иначе это значение
        может быть подменено и не вернуть реальной значение физического порта.
        Полагаться на это значение небезопасно в контексте приложений, требующих
        усиленной безопасности.
       </span>
            </p></blockquote>
    </dd>




    <dt>
        '<var class="varname">SERVER_SIGNATURE</var>'</dt>

    <dd>

      <span class="simpara">
       Строка, содержащая версию сервера и имя виртуального хоста, которые
       добавляются к генерируемым сервером страницам, если включено.
      </span>
    </dd>




    <dt>
        '<var class="varname">PATH_TRANSLATED</var>'</dt>

    <dd>

      <span class="simpara">
       Путь файловой системы (не document root) к текущему скрипту, после того как сервер выполнил отображение virtual-to-real.

      </span>
        <blockquote class="note"><p><strong class="note">Замечание</strong>:
                <span class="simpara">
        Пользователи Apache 2 могут использовать директиву
        <code class="literal">AcceptPathInfo = On</code> в конфигурационном файле
        <var class="filename">httpd.conf</var> для задания переменной <var class="envar">PATH_INFO</var>.
       </span>
            </p></blockquote>
    </dd>




    <dt>
        '<var class="varname">SCRIPT_NAME</var>'</dt>

    <dd>

      <span class="simpara">
       Содержит путь к текущему исполняемому скрипту. Это полезно для страниц,
       которые должны указывать на самих себя.
       Константа <a href="language.constants.predefined.php" class="link">__FILE__</a>
       содержит полный путь и имя текущего (то есть включённого) файла.
      </span>
    </dd>




    <dt>
        '<var class="varname">REQUEST_URI</var>'</dt>

    <dd>

      <span class="simpara">
       URI, который был предоставлен для доступа к этой странице.
       Например, '<code class="literal">/index.html</code>'.
      </span>
    </dd>




    <dt>
        '<var class="varname">PHP_AUTH_DIGEST</var>'</dt>

    <dd>

      <span class="simpara">
       При выполнении аутентификации HTTP Digest этой переменной присваивается
       заголовок 'Authorization', отправленный клиентом (который затем следует
       использовать для проведения соответствующей проверки).
      </span>
    </dd>




    <dt>
        '<var class="varname">PHP_AUTH_USER</var>'</dt>

    <dd>

      <span class="simpara">
       При выполнении HTTP-аутентификации этой переменной присваивается
       имя пользователя, предоставленное пользователем.
      </span>
    </dd>

    <dt>
        '<var class="varname">PHP_AUTH_PW</var>'</dt>

    <dd>

      <span class="simpara">
       При выполнении HTTP-аутентификации этой переменной присваивается
       пароль, предоставленный пользователем.
      </span>
    </dd>

    <dt>
        '<var class="varname">AUTH_TYPE</var>'</dt>

    <dd>

      <span class="simpara">
       При выполнении HTTP-аутентификации этой переменной присваивается
       тип аутентификации, который используется.
      </span>
    </dd>

    <dt>
        '<var class="varname">PATH_INFO</var>'</dt>
    <dd>
      <span class="simpara">
       Содержит любой предоставленный пользователем путь, содержащийся после
       имени скрипта, но до строки запроса, если она есть.
       Например, если текущий скрипт запрошен по URL
       <var class="filename">http://www.example.com/php/path_info.php/some/stuff?foo=bar</var>,
       то переменная <var class="varname">$_SERVER['PATH_INFO']</var> будет содержать
       <code class="literal">/some/stuff</code>.
      </span>
    </dd>

    <dt>
        '<var class="varname">ORIG_PATH_INFO</var>'</dt>
    <dd>
      <span class="simpara">
       Исходное значение переменной '<var class="varname">PATH_INFO</var>' перед
       обработкой PHP.
      </span>
    </dd>
</dl>

</body>
</html>
