<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/variables.php">Назад</a></li>
</ul>

<h1>Область видимости переменной</h1>

<p>Область видимости переменной - это контекст, в котором эта переменная определена. В большинстве случаев все переменные PHP имеют только одну область видимости. Эта единая область видимости охватывает также включаемые (include) и требуемые (require) файлы.</p>

<p>Функция задаёт локальную область видимости. Любая используемая внутри функции переменная по умолчанию ограничена локальной областью видимости функции.</p>

<h2>Ключевое слово global</h2>

<pre>
    &lt;?php
    $a = 1;
    $b = 2;

    function Sum()
    {
        global $a, $b;

        $b = $a + $b;
    }

    Sum();
    echo $b; // 3
    ?&gt;
</pre>

<p>Второй способ доступа к переменным глобальной области видимости - использование специального, определяемого PHP массива $GLOBALS.</p>

<pre>
    &lt;?php
    $a = 1;
    $b = 2;

    function Sum()
    {
        $GLOBALS['b'] = $GLOBALS['a'] + $GLOBALS['b'];
    }

    Sum();
    echo $b; // 3
    ?&gt;
</pre>

<p><b>Замечание:</b> Использование ключевого слова global вне функции не является ошибкой. Оно может быть использовано в файле, который включается внутри функции.</p>

<h2>Использование статических (static) переменных</h2>

<pre>
    &lt;?php
    function test()
    {
        static $a = 0;
        echo $a;
        $a++;
    }
    ?&gt;
</pre>

<p>$a будет проинициализирована только при первом вызове функции, а каждый вызов функции test() будет выводить значение $a и инкрементировать его.</p>

<pre>
<?php
$form = file_get_contents(__DIR__ . "/examples/scope-fun-in.txt");
echo htmlentities($form);
?>
</pre>

<p><b>Замечание:</b> Статические объявления вычисляются во время компиляции скрипта.</p>

<h2>Ссылки с глобальными (global) и статическими (static) переменными</h2>

<p>PHP использует модификаторы переменных static и global как ссылки.</p>

<pre>
<?php
$form = file_get_contents(__DIR__ . "/examples/scope-ref.php");
echo htmlentities($form);
?>
</pre>

<p>При присвоении ссылки статической переменной она <em>не запоминается</em></p>

</body>
</html>
