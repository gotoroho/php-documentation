<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/control-structures.php">Назад</a></li>
</ul>

<h1>include_once</h1>

<p>Выражение include_once включает и выполняет указанный файл во время выполнения скрипта. Его поведение идентично выражению include, с той лишь разницей, что если код из файла уже один раз был включён, он не будет включён и выполнен повторно и вернёт true. Как видно из имени, он включит файл только один раз.</p>

<p>include_once может использоваться в тех случаях, когда один и тот же файл может быть включён и выполнен более одного раза во время выполнения скрипта, в данном случае это поможет избежать проблем с переопределением функций, переменных и т.д.</p>

<p>Смотрите документацию по include для получения информации о том, как эта функция работает.</p>

</body>
</html>
