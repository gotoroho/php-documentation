<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/types.php">Назад</a></li>
</ul>

<h1>Ресурс</h1>

<p>Ресурс - это переменная, содержащая ссылку на внешний ресурс. Создаются только специальными функциями.</p>

<p>Определение отсутствия ссылок на ресурс происходит автоматически, после чего он освобождается сборщиком мусора.</p>

<p><b>Замечание:</b> Постоянные соединения с базами данных являются исключением из этого правила. Они не уничтожаются сборщиком мусора.</p>

</body>
</html>
