<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/namespaces.php">Назад</a></li>
</ul>

<h1>Обзор пространств имён</h1>

<p>Что такое пространства имён? В широком смысле - это один из способов инкапсуляции элементов. Такое абстрактное понятие можно увидеть во многих местах. Например, в любой операционной системе директории служат для группировки связанных файлов и выступают в качестве пространства имён для находящихся в них файлов. В качестве конкретного примера файл foo.txt может находиться сразу в обеих директориях: /home/greg и /home/other, но две копии foo.txt не могут существовать в одной директории. Кроме того, для доступа к foo.txt извне директории /home/greg, мы должны добавить имя директории перед именем файла используя разделитель, чтобы получить /home/greg/foo.txt. Этот же принцип распространяется и на пространства имён в программировании.</p>

<figure>
    <figcaption>В PHP пространства имён используются для решения двух проблем, с которыми сталкиваются авторы библиотек и приложений при создании повторно используемых элементов кода, таких как классы и функции:</figcaption>

    <ol>
        <li>Конфликт имён между вашим кодом и внутренними классами/функциями/константами PHP или сторонними.</li>
        <li>Возможность создавать псевдонимы (или сокращения) для Ну_Очень_Длинных_Имён, чтобы облегчить первую проблему и улучшить читаемость исходного кода.</li>
    </ol>
</figure>

<p><b>Замечание:</b> Имена пространств имён регистронезависимы.</p>

<p><b>Замечание:</b> Названия пространств имён PHP и составные названия, начинающиеся с этих (такие как PHP\Classes), являются зарезервированными для нужд языка и их не следует использовать в пользовательском коде.</p>

</body>
</html>
