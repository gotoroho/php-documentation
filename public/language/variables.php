<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Переменные</h1>

<ul>
    <li><a href="/language/variables/basics.php">Основы</a></li>
    <li><a href="/language/variables/predefined.php">Предопределённые переменные</a></li>
    <li><a href="/language/variables/scope.php">Область видимости переменной</a></li>
    <li><a href="/language/variables/variable.php">Переменные переменных</a></li>
    <li><a href="/language/variables/external.php">Переменные извне PHP</a></li>
</ul>

</body>
</html>
