<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/context.php">Назад</a></li>
</ul>

<h1>Контекстные опции сокета</h1>

<p>Контекстные опции доступны для всех обёрток, которые работают через сокеты, такие как tcp, http и ftp.</p>

<dl>
    <dt><b>bindto</b></dt>
    <dd>
        <p>Используется для указания IP-адреса (IPv4 или IPv6) и\или номера порта, которые PHP будет использовать для
            подключения к сети. Синтаксис выглядитследующим образом: <code>ip:port</code> для адреса IPv4, и
            <code>[ip]:port</code> для адреса IPv6. Установка IP и\или порта в <code>0</code> позволит системе самой
            выбрать нужный IP и\или порт.</p>
        <p><b>Замечание:</b> Так как во время обычной работы FTP создаёт 2 соединения с сокетами, номер порта не может
            быть задан с помощью данной опции.</p>
    </dd>

    <dt><b>backlog</b></dt>
    <dd>
        <p>Используется для ограничения исходящих соединений в очереди соединений сокета. </p>
        <p><b>Замечание:</b> Используется только для stream_socket_server().</p>
    </dd>

    <dt><b>ipv6_v6only</b></dt>
    <dd>
        <p>Переопределяет значение ОС по умолчанию для отображения IPv4 в IPv6.</p>
        <p><b>Замечание:</b> Это важно в случае попытки отдельно слушать IPv4 адреса, в то время как задана привязка к
            <code>[::]</code>. Применимо только к stream_socket_server().</p>
    </dd>

    <dt><b>so_reuseport</b></dt>
    <dd>
        <p>Позволяет множественную привязку к одной и той же паре IP:порт, даже из разных процессов.</p>
        <p><b>Замечание:</b> Применимо только к stream_socket_server().</p>
    </dd>

    <dt><b>so_broadcast</b></dt>
    <dd>
        <p>Разрешает посылать и принимать данные в/от широковещательных адресов. </p>
        <p><b>Замечание:</b> Применимо только к stream_socket_server().</p>
    </dd>

    <dt><b>tcp_nodelay</b></dt>
    <dd>
        <p>Установка этой опции в true сделает <code>SOL_TCP,NO_DELAY=1</code> соответственно, таким образом, отключение
            алгоритма TCP Nagle.</p>
    </dd>
</dl>

<pre>
    &lt;?php
    // Соединение с сетью, используя IP '192.168.0.100'
    $opts = array(
        'socket' => array(
            'bindto' => '192.168.0.100:0',
        ),
    );


    // Соединение с сетью, используя IP '192.168.0.100' и порт '7000'
    $opts = array(
        'socket' => array(
            'bindto' => '192.168.0.100:7000',
        ),
    );


    // Соединение с сетью, используя IPv6 адрес '2001:db8::1'
    // и порт '7000'
    $opts = array(
        'socket' => array(
            'bindto' => '[2001:db8::1]:7000',
        ),
    );


    // Соединение с сетью через порт '7000'
    $opts = array(
        'socket' => array(
            'bindto' => '0:7000',
        ),
    );


    // Создаём контекст...
    $context = stream_context_create($opts);

    // ...и используем его для получения данных
    echo file_get_contents('http://www.example.com', false, $context);
</pre>

</body>
</html>
