<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/control-structures.php">Назад</a></li>
</ul>

<h1>elseif/else if</h1>

<p>Конструкция elseif, как её имя и говорит есть сочетание if и else. Аналогично else, она расширяет оператор if для выполнения различных выражений в случае, когда условие начального оператора if эквивалентно false. Однако, в отличие от else, выполнение альтернативного выражения произойдёт только тогда, когда условие оператора elseif будет являться равным true.</p>

<p>Может быть несколько elseif в одном выражении if. Первое выражение elseif (если оно есть) равное true будет выполнено. В PHP вы также можете написать 'else if' (в два слова), и тогда поведение будет идентичным 'elseif' (в одно слово). Синтаксически значение немного отличается (если вы знакомы с языком С, это то же самое поведение), но в конечном итоге оба выражения приведут к одному и тому же результату.</p>

<p>Выражение elseif выполнится, если предшествующее выражение if и предшествующие выражения elseif эквивалентны false, а текущий elseif равен true.</p>

<p><b>Замечание:</b> Заметьте, что elseif и else if будут равнозначны только при использовании фигурных скобок, как в примерах выше. Если используются синтаксис с двоеточием для определения условий if/elseif, вы не должны разделять else if на два слова, иначе это вызовет фатальную ошибку в PHP.</p>

</body>
</html>
