<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/exceptions.php">Назад</a></li>
</ul>

<h1>Наследование исключений</h1>

<p>Определённый пользователем класс исключения должен быть определён, как класс расширяющий (наследующий) встроенный класс Exception. Ниже приведены методы и свойства класса Exception, доступные дочерним классам.</p>

<pre>
    &lt;?php
    class Exception implements Throwable
    {
        protected $message = 'Unknown exception';   // сообщение об исключении
        private   $string;                          // свойство для __toString
        protected $code = 0;                        // пользовательский код исключения
        protected $file;                            // файл, в котором было выброшено исключение
        protected $line;                            // строка, в которой было выброшено исключение
        private   $trace;                           // трассировка вызовов методов и функций
        private   $previous;                        // предыдущее исключение, если исключение вложенное

        public function __construct($message = '', $code = 0, Throwable $previous = null);

        final private function __clone();           // запрещает клонирования исключения

        final public  function getMessage();        // сообщение исключения
        final public  function getCode();           // код исключения
        final public  function getFile();           // файл, где выброшено исключение
        final public  function getLine();           // строка, на которой выброшено исключение
        final public  function getTrace();          // массив backtrace()
        final public  function getPrevious();       // предыдущее исключение
        final public  function getTraceAsString();  // отформатированная строка трассировки

        // Переопределяемый
        public function __toString();               // отформатированная строка для отображения
    }
    ?&gt;
</pre>

<p>Если класс, наследуемый от Exception переопределяет конструктор, необходимо вызвать в конструкторе parent::__construct(), чтобы быть уверенным, что все доступные данные были правильно присвоены. Метод __toString() может быть переопределён, чтобы обеспечить нужный вывод, когда объект преобразуется в строку.</p>

<p><b>Замечание:</b> Исключения нельзя клонировать. Попытка клонировать исключение приведёт к неисправимой ошибке E_ERROR.</p>

<pre>
    &lt;?php
    /**
     * Определим свой класс исключения
     */
    class MyException extends Exception
    {
        // Переопределим исключение так, что параметр message станет обязательным
        public function __construct($message, $code = 0, Throwable $previous = null) {
            // некоторый код

            // убедитесь, что все передаваемые параметры верны
            parent::__construct($message, $code, $previous);
        }

        // Переопределим строковое представление объекта.
        public function __toString() {
            return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }

        public function customFunction() {
            echo "Мы можем определять новые методы в наследуемом классе\n";
        }
    }


    /**
     * Создадим класс для тестирования исключения
     */
    class TestException
    {
        public $var;

        const THROW_NONE    = 0;
        const THROW_CUSTOM  = 1;
        const THROW_DEFAULT = 2;

        function __construct($avalue = self::THROW_NONE) {

            switch ($avalue) {
                case self::THROW_CUSTOM:
                    // Выбрасываем собственное исключение
                    throw new MyException('1 - неправильный параметр', 5);
                    break;

                case self::THROW_DEFAULT:
                    // Выбрасываем встроеное исключение
                    throw new Exception('2 - недопустимый параметр', 6);
                    break;

                default:
                    // Никаких исключений, объект будет создан.
                    $this->var = $avalue;
                    break;
            }
        }
    }


    // Пример 1
    try {
        $o = new TestException(TestException::THROW_CUSTOM);
    } catch (MyException $e) {      // Будет перехвачено
        echo "Поймано собственное переопределённое исключение\n", $e;
        $e->customFunction();
    } catch (Exception $e) {        // Будет пропущено
        echo "Поймано встроенное исключение\n", $e;
    }

    // Отсюда будет продолжено выполнение программы
    var_dump($o); // Null
    echo "\n\n";


    // Пример 2
    try {
        $o = new TestException(TestException::THROW_DEFAULT);
    } catch (MyException $e) {      // Тип исключения не совпадёт
        echo "Поймано переопределённое исключение\n", $e;
        $e->customFunction();
    } catch (Exception $e) {        // Будет перехвачено
        echo "Перехвачено встроенное исключение\n", $e;
    }

    // Отсюда будет продолжено выполнение программы
    var_dump($o); // Null
    echo "\n\n";


    // Пример 3
    try {
        $o = new TestException(TestException::THROW_CUSTOM);
    } catch (Exception $e) {        // Будет перехвачено
        echo "Поймано встроенное исключение\n", $e;
    }

    // Продолжение исполнения программы
    var_dump($o); // Null
    echo "\n\n";


    // Пример 4
    try {
        $o = new TestException();
    } catch (Exception $e) {        // Будет пропущено, т.к. исключение не выбрасывается
        echo "Поймано встроенное исключение\n", $e;
    }

    // Продолжение выполнения программы
    var_dump($o); // TestException
    echo "\n\n";
    ?&gt;
</pre>

</body>
</html>
