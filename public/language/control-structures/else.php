<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/control-structures.php">Назад</a></li>
</ul>

<h1>else</h1>

<p>Часто необходимо выполнить одно выражение, если определённое условие верно, и другое выражение, если условие не верно. Именно для этого else и используется. else расширяет оператор if, чтобы выполнить выражение, в случае, если условие в операторе if равно false.</p>

<p>Выражение else выполняется только, если выражение if вычисляется как false, и если нет других любых выражений elseif, или если они все равны false также (смотрите elseif).</p>

</body>
</html>
