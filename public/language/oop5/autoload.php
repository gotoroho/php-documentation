<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Автоматическая загрузка классов</h1>

<p>Большинство разработчиков объектно-ориентированных приложений используют такое соглашение именования файлов, в котором каждый класс хранится в отдельно созданном для него файле. Одна из самых больших неприятностей - необходимость писать в начале каждого скрипта длинный список подгружаемых файлов (по одному для каждого класса).</p>

<p>Функция spl_autoload_register() позволяет зарегистрировать необходимое количество автозагрузчиков для автоматической загрузки классов и интерфейсов, если они в настоящее время не определены. Регистрируя автозагрузчики, PHP получает последний шанс для интерпретатора загрузить класс прежде, чем он закончит выполнение скрипта с ошибкой.</p>

<p><b>Предостережение</b> До PHP 8.0.0 можно было использовать __autoload() для автозагрузки классов и интерфейсов. Однако это менее гибкая альтернатива spl_autoload_register(), функция __autoload() объявлена устаревшей в PHP 7.2.0 и удалена в PHP 8.0.0.</p>

<pre>
    &lt;?php
    spl_autoload_register(function ($class_name) {
        include $class_name . '.php';
    });

    $obj  = new MyClass1();
    $obj2 = new MyClass2();
    ?&gt;
</pre>

<pre>
    &lt;?php
    spl_autoload_register(function ($name) {
        echo "Хочу загрузить $name.\n";
        throw new MissingException("Невозможно загрузить $name.");
    });

    try {
        $obj = new NonLoadableClass();
    } catch (Exception $e) {
        echo $e->getMessage(), "\n";
    }
    ?&gt;
</pre>

</body>
</html>
