<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/control-structures.php">Назад</a></li>
</ul>

<h1>Альтернативный синтаксис управляющих структур</h1>

<p>PHP предлагает альтернативный синтаксис для некоторых его управляющих структур, а именно: if, while, for, foreach и switch. В каждом случае основной формой альтернативного синтаксиса является изменение открывающей фигурной скобки на двоеточие (:), а закрывающей скобки на endif;, endwhile;, endfor;, endforeach; или endswitch; соответственно.</p>

<p><b>Замечание:</b> Смешивание синтаксиса в одном и том же блоке управления не поддерживается.</p>

<p><b>Внимание</b> Любой вывод (включая пробельные символы) между выражением switch и первым case приведут к синтаксической ошибке.</p>

<p>В то же время следующий пример будет работать, так как завершающий перевод строки после выражения switch считается частью закрывающего ?> и следовательно ничего не выводится между switch и case:</p>

<pre>
    &lt;?php switch ($foo): ?&gt;
    &lt;?php case 1: ?&gt;
        ...
    &lt;?php endswitch ?&gt;
</pre>

</body>
</html>
