<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/references.php">Назад</a></li>
</ul>

<h1>Чем ссылки не являются</h1>

<p>Как уже было сказано, ссылки не являются указателями. Это означает, что следующая конструкция не будет делать то, что вы ожидаете:</p>

<pre>
    &lt;?php
    function foo(&$var) {
        $var =& $GLOBALS["baz"];
    }
    foo($bar);
    ?&gt;
</pre>

<p>Переменная $var в функции foo будет связана с $bar в вызывающем коде, но затем она будет перепривязана к $GLOBALS["baz"]. Нет способа связать $bar в области видимости вызывающем коде с чем-либо ещё путём использования механизма ссылок, поскольку $bar не доступна в функции foo (доступно лишь её значение через $var, но $var имеет только значение переменной и не имеет связи имя-значение в таблице имён переменных). Вы можете воспользоваться возвратом ссылок из функции для привязки внешней переменной к другому значению.</p>

</body>
</html>
