<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/context.php">Назад</a></li>
</ul>

<h1>Опции контекста HTTP</h1>

<p>Опции контекста для транспортных протоколов http:// и https://.</p>

<dl>
    <dt>method string</dt>
    <dd>GET, POST или любой другой метод HTTP, поддерживаемый удалённым сервером. По умолчанию GET.</dd>

    <dt>header array или string</dt>
    <dd>Дополнительные заголовки для отправки вместе с запросом. Значения в этой опции будут переопределять другие значения (такие как User-agent:, Host: и Authentication:), даже при следующих переадресациях Location:. Таким образом, не рекомендуется устанавливать заголовок Host:, если включён параметр follow_location.</dd>

    <dt>user_agent string</dt>
    <dd>Значение для отправки вместе с заголовком User-Agent:. Это значение будет использоваться, если заголовок user-agent не был указан в опции контекста header выше. По умолчанию используется значение директивы user_agent из файла php.ini.</dd>

    <dt>content string</dt>
    <dd>Дополнительные данные для отправки после заголовков. Обычно используется с запросами POST и PUT.</dd>

    <dt>proxy string</dt>
    <dd>URI, указывающий адрес прокси-сервера. (Например, tcp://proxy.example.com:5100).</dd>

    <dt>request_fulluri bool</dt>
    <dd>Когда установлено в true, весь URI будет использован при формировании запроса. (Например, GET http://www.example.com/path/to/file.html HTTP/1.0). Хотя это нестандартный формат запроса, некоторые прокси-серверы требуют его. По умолчанию false.</dd>

    <dt>follow_location int</dt>
    <dd>Следовать переадресациям заголовка Location. Для отключения установите в значение 0. По умолчанию 1.</dd>

    <dt>max_redirects int</dt>
    <dd>Максимальное количество переадресаций, которым можно следовать. Значение 1 или меньше означает, что никаких переадресаций не будет произведено. По умолчанию 20.</dd>

    <dt>protocol_version float</dt>
    <dd>Версия протокола HTTP. По умолчанию 1.0.</dd>

    <dt>timeout float</dt>
    <dd>Тайм-аут на чтение в секундах, указанный с помощью типа float (например, 10.5). По умолчанию используется значение директивы default_socket_timeout из файла php.ini.</dd>

    <dt>ignore_errors bool</dt>
    <dd>Извлечь содержимое даже при неуспешных статусах завершения. По умолчанию false.</dd>
</dl>

<pre>
    &lt;?php
    $postdata = http_build_query(
        array(
            'var1' => 'некоторое содержимое',
            'var2' => 'doh'
        )
    );

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );

    $context = stream_context_create($opts);

    $result = file_get_contents('http://example.com/submit.php', false, $context);


    $url = "http://www.example.org/header.php";

    $opts = array('http' =>
        array(
            'method' => 'GET',
            'max_redirects' => '0',
            'ignore_errors' => '1'
        )
    );

    $context = stream_context_create($opts);
    $stream = fopen($url, 'r', false, $context);

    // информация о заголовках, а также
    // метаданные о потоке
    var_dump(stream_get_meta_data($stream));

    // актуальная информация по ссылке $url
    var_dump(stream_get_contents($stream));
    fclose($stream);
</pre>

<p><b>Замечание:</b> Дополнительные опции контекста могут поддерживаться нижележащим транспортным протоколом. Для потоков http://, это относится к опциям контекста для транспортного протокола tcp://. Для потоков https://, это относится к опциям контекста для транспортного протокола ssl://.</p>

<p><b>Замечание:</b> Когда эта обёртка потока следует по переадресации, wrapper_data, возвращаемый функцией stream_get_meta_data(), необязательно содержит строку статуса HTTP, которая на самом деле относится к содержанию данных по индексу 0. Первый запрос вернул код 301 (постоянное перенаправление), так что обёртка потока автоматически последовала этому перенаправлению, чтобы получить ответ 200 (индекс = 4).</p>

</body>
</html>
