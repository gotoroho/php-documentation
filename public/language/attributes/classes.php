<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/attributes.php">Назад</a></li>
</ul>

<h1>Объявление классов атрибутов</h1>

<p>Создавать классы для атрибутов не обязательно, но крайне рекомендуется. В самом простом случае, требуется просто пустой класс с атрибутом #[Attribute], который можно импортировать из глобального пространства имён с помощью оператора use.</p>

<pre>
    &lt;?php

    namespace Example;

    use Attribute;

    #[Attribute]
    class MyAttribute
    {
    }
</pre>

<p>Для ограничения того, с каким типом деклараций можно использовать конкретный атрибут, можно передать битовую маску первым параметром в #[Attribute].</p>

<pre>
    &lt;?php

    namespace Example;

    use Attribute;

    #[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_FUNCTION)]
    class MyAttribute
    {
    }
</pre>

<p>После этого, аннотирование атрибутом MyAttribute чего либо, отличного от метода или функции приведёт к выбросу исключения при вызове ReflectionAttribute::newInstance()</p>

<p>По умолчанию, атрибут можно использовать только один раз для каждой сущности. Если нужна возможность указывать несколько одинаковых атрибутов для одной сущности - можно выставить соответствующий флаг в битовой маске для декларации #[Attribute].</p>

<pre>
    &lt;?php

    namespace Example;

    use Attribute;

    #[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_FUNCTION | Attribute::IS_REPEATABLE)]
    class MyAttribute
    {
    }
</pre>

</body>
</html>
