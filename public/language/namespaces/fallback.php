<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/namespaces.php">Назад</a></li>
</ul>

<h1>Использование пространств имён: переход к глобальной функции/константе</h1>

<p>Внутри пространства имён, когда PHP встречает неполное имя класса, функции или константы, он преобразует эти имена с разными приоритетами. Имена классов всегда преобразуются к текущему имени пространства имён. Таким образом, чтобы получить доступ ко внутреннему классу или пользовательскому классу вне пространства имён, необходимо ссылаться по их абсолютному имени.</p>

<pre>
    &lt;?php
    namespace A\B\C;
    class Exception extends \Exception {}

    $a = new Exception('hi'); // $a - это объект класса A\B\C\Exception
    $b = new \Exception('hi'); // $b - это объект класса Exception

    $c = new ArrayObject; // фатальная ошибка, класс A\B\C\ArrayObject не найден
    ?&gt;
</pre>

<p>Для функций и констант, PHP будет прибегать к глобальным функциям или константам, если функция или константа не существует в пространстве имён.</p>

<pre>
    &lt;?php
    namespace A\B\C;

    const E_ERROR = 45;
    function strlen($str)
    {
        return \strlen($str) - 1;
    }

    echo E_ERROR, "\n"; // выводит "45"
    echo INI_ALL, "\n"; // выводит "7" - прибегнет к глобальной INI_ALL

    echo strlen('hi'), "\n"; // выводит "1"
    if (is_array('hi')) { // выводит строку "это не массив"
        echo "это массив\n";
    } else {
        echo "это не массив\n";
    }
    ?&gt;
</pre>

</body>
</html>
