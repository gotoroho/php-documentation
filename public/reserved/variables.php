<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Предопределённые переменные</h1>

<p>PHP предоставляет всем скриптам большое количество предопределённых переменных. Эти переменные содержат всё, от внешних данных до переменных среды окружения, от текста сообщений об ошибках до последних полученных заголовков.</p>

<ul>
    <li><a href="/reserved/variables/superglobals.php">Суперглобальные переменные</a> — Суперглобальные переменные - это встроенные переменные, которые всегда доступны во всех областях видимости</li>
    <li><a href="/reserved/variables/globals.php">$GLOBALS</a> — Ссылки на все переменные глобальной области видимости</li>
    <li><a href="/reserved/variables/server.php">$_SERVER</a> — Информация о сервере и среде исполнения</li>
    <li><a href="/reserved/variables/get.php">$_GET</a> — Переменные HTTP GET</li>
    <li><a href="/reserved/variables/post.php">$_POST</a> — Переменные HTTP POST</li>
    <li><a href="/reserved/variables/files.php">$_FILES</a> — Переменные файлов, загруженных по HTTP</li>
    <li><a href="/reserved/variables/request.php">$_REQUEST</a> — Переменные HTTP-запроса</li>
    <li><a href="/reserved/variables/session.php">$_SESSION</a> — Переменные сессии</li>
    <li><a href="/reserved/variables/environment.php">$_ENV</a> — Переменные окружения</li>
    <li><a href="/reserved/variables/cookies.php">$_COOKIE</a> — HTTP Cookies</li>
    <li><a href="/reserved/variables/phperrormsg.php">$php_errormsg</a> — Предыдущее сообщение об ошибке (DEPRECATED)</li>
    <li><a href="/reserved/variables/httpresponseheader.php">$http_response_header</a> — Заголовки ответов HTTP</li>
    <li><a href="/reserved/variables/argc.php">$argc</a> — Количество аргументов, переданных скрипту</li>
    <li><a href="/reserved/variables/argv.php">$argv</a> — Массив переданных скрипту аргументов</li>
</ul>

</body>
</html>
