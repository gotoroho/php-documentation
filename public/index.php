<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h1>Содержание</h1>

<ul>
    <li><a href="/language/basic-syntax.php">Основы синтаксиса</a></li>
    <li><a href="/language/types.php">Типы</a></li>
    <li><a href="/language/variables.php">Переменные</a></li>
    <li><a href="/language/constants.php">Константы</a></li>
    <li><a href="/language/expressions.php">Выражения</a></li>
    <li><a href="/language/operators.php">Операторы</a></li>
    <li><a href="/language/control-structures.php">Управляющие конструкции</a></li>
    <li><a href="/language/functions.php">Функции</a></li>
    <li><a href="/language/oop5.php">Классы и объекты</a></li>
    <li><a href="/language/namespaces.php">Пространства имён</a></li>
    <li><a href="/language/errors.php">Ошибки</a></li>
    <li><a href="/language/exceptions.php">Исключения</a></li>
    <li><a href="/language/generators.php">Генераторы</a></li>
    <li><a href="/language/attributes.php">Атрибуты</a></li>
    <li><a href="/language/references.php">Объяснение ссылок</a></li>
    <li><a href="/reserved/variables.php">Предопределённые переменные</a></li>
    <li><a href="/reserved/exceptions.php">Предопределённые исключения</a></li>
    <li><a href="/reserved/interfaces.php">Встроенные интерфейсы и классы</a></li>
    <li><a href="/context.php">Контекстные опции и параметры</a></li>
    <li><a href="/wrappers.php">Поддерживаемые протоколы и обёртки</a></li>
</ul>

</body>
</html>
