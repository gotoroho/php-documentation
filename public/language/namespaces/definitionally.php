<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/namespaces.php">Назад</a></li>
</ul>

<h1>Описание нескольких пространств имён в одном файле</h1>

<p>Несколько пространств имён также можно описать в одном файле с помощью двух допустимых синтаксических конструкций.</p>

<pre>
    &lt;?php
    // 1st way
    namespace MyProject;

    const CONNECT_OK = 1;
    class Connection { /* ... */ }
    function connect() { /* ... */  }

    namespace AnotherProject;

    const CONNECT_OK = 1;
    class Connection { /* ... */ }
    function connect() { /* ... */  }

    // 2nd way
    namespace MyProject {
        const CONNECT_OK = 1;
        class Connection { /* ... */ }
        function connect() { /* ... */  }
    }

    namespace AnotherProject {
        const CONNECT_OK = 1;
        class Connection { /* ... */ }
        function connect() { /* ... */  }
    }
    ?&gt;
</pre>

<p>Настоятельно не рекомендуется при программировании комбинировать несколько пространств имён в один файл. Основным применением этому может быть объединение нескольких PHP-файлов в один файл.</p>

<p>Для объединения кода в глобальном пространстве имён с кодом в других пространствах имён, используется только синтаксис со скобками. Глобальный код должен быть помещён в конструкцию описания пространства имён без указания имени:</p>

<pre>
    &lt;?php
    namespace MyProject {
        const CONNECT_OK = 1;
        class Connection { /* ... */ }
        function connect() { /* ... */  }
    }

    namespace {       // глобальный код
        session_start();
        $a = MyProject\connect();
        echo MyProject\Connection::start();
    }
    ?&gt;
</pre>

</body>
</html>
