<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Контекстные опции и параметры</h1>

<p>PHP предлагает различные контекстные опции и параметры, которые могут быть использованы со всеми файловыми системами и обёртками потоков. Контекст создаётся с помощью функции stream_context_create(). Опции устанавливаются с помощью stream_context_set_option(), а параметры с помощью stream_context_set_params().</p>

<ul>
    <li><a href="/context/socket.php">Контекстные опции сокета</a></li>
    <li><a href="/context/http.php">Опции контекста HTTP</a></li>
    <li><a href="/context/ftp.php">Параметры контекста FTP</a></li>
    <li><a href="/context/ssl.php">Опции контекста SSL</a></li>
    <li><a href="/context/curl.php">Опции контекста CURL</a></li>
    <li><a href="/context/phar.php">Контекстные опции Phar</a></li>
    <li><a href="/context/params.php">Параметры контекста</a></li>
    <li><a href="/context/zip.php">Опции контекста Zip</a></li>
</ul>

</body>
</html>
