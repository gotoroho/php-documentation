<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/namespaces.php">Назад</a></li>
</ul>

<h1>Глобальное пространство</h1>

<p>Без определения пространства имён, определения всех классов и функций находятся в глобальном пространстве - так же, как это было в PHP до введения пространств имён. Добавление префикса \ к именам означает, что это имя должно находиться в глобальном пространстве, даже если вы находитесь в контексте определённого пространства имён.</p>

<pre>
    &lt;?php
    namespace A\B\C;

    /* Эта функция является A\B\C\fopen */
    function fopen() {
        /* ... */
        $f = \fopen(...); // вызов глобальной функции fopen
        return $f;
    }
    ?&gt;
</pre>

</body>
</html>
