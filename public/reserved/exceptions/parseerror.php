<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/exceptions.php">Назад</a></li>
</ul>

<h1>ParseError</h1>

<p>ParseError выбрасывается, когда возникает ошибка при разборе PHP-кода, например, когда вызывается функция eval().</p>

<p><b>Замечание:</b> Начиная с PHP 7.3.0, класс ParseError наследуется от CompileError. Ранее этот класс расширял класс Error.</p>

</body>
</html>
