<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Итераторы объектов</h1>

<p>PHP предоставляет такой способ объявления объектов, который даёт возможность пройти по списку элементов данного объекта, например, с помощью оператора foreach. По умолчанию, в этом обходе
    (итерации) будут участвовать все видимые свойства объекта.</p>

<pre>
    &lt;?php
    class MyClass
    {
        public $var1 = 'значение 1';
        public $var2 = 'значение 2';
        public $var3 = 'значение 3';

        protected $protected = 'защищённая переменная';
        private   $private   = 'закрытая переменная';

        function iterateVisible() {
            echo "MyClass::iterateVisible:\n";
            foreach ($this as $key => $value) {
                print "$key => $value\n";
            }
        }
    }

    $class = new MyClass();

    foreach($class as $key => $value) {
        print "$key => $value\n";
    }
    echo "\n";


    $class->iterateVisible();

    ?&gt;
</pre>

<small>
    var1 => значение 1<br>
    var2 => значение 2<br>
    var3 => значение 3<br>
    <br>
    MyClass::iterateVisible:<br>
    var1 => значение 1<br>
    var2 => значение 2<br>
    var3 => значение 3<br>
    protected => защищённая переменная<br>
    private => закрытая переменная
</small>

</body>
</html>
