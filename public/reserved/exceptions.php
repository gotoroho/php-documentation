<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Предопределённые исключения</h1>

<ul>
    <li><a href="/reserved/exceptions/exception.php">Exception</a></li>
    <li><a href="/reserved/exceptions/errorexception.php">ErrorException</a></li>
    <li><a href="/reserved/exceptions/error.php">Error</a></li>
    <li><a href="/reserved/exceptions/argumentcounterror.php">ArgumentCountError</a></li>
    <li><a href="/reserved/exceptions/arithmeticerror.php">ArithmeticError</a></li>
    <li><a href="/reserved/exceptions/assertionerror.php">AssertionError</a></li>
    <li><a href="/reserved/exceptions/divisionbyzeroerror.php">DivisionByZeroError</a></li>
    <li><a href="/reserved/exceptions/compileerror.php">CompileError</a></li>
    <li><a href="/reserved/exceptions/parseerror.php">ParseError</a></li>
    <li><a href="/reserved/exceptions/typeerror.php">TypeError</a></li>
    <li><a href="/reserved/exceptions/valueerror.php">ValueError</a></li>
    <li><a href="/reserved/exceptions/unhandledmatcherror.php">UnhandledMatchError</a></li>
</ul>

</body>
</html>
