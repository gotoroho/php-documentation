<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/types.php">Назад</a></li>
</ul>

<h1>Callable</h1>

<p>Можно использовать любые встроенные, либо созданные пользователем функции, за исключением конструкций языка, таких как: array(), echo, empty(), eval(), exit(), isset(), list(), print или unset().</p>

<p>Если объект реализует __invoke(), то он callable.</p>

<p>Вместо callable функции можно передать массив, где первый аргумент - это имя класса или экзкмпляр класса, а второй - название вызываемой в классе функции.</p>

<pre>
    &lt;?php
    function getClosure(): Closure
    {
        $g = 'test';

        return function($a, $b) use($g){
            echo $a . $b . $g;
        };
    }

    $closure = getClosure();
    $closure(1, 3); //13test
    echo "&lt;br&gt;";

    getClosure()->__invoke(1, 3); //13test
    echo "&lt;br&gt;";

    echo "&lt;textarea rows='15' cols='50'&gt;";
    var_dump($closure);
    echo "&lt;/textarea&gt;";
    ?&gt;
</pre>

<small>
    <?php
    function getClosure(): Closure
    {
        $g = 'test';

        return function($a, $b) use($g){
            echo $a . $b . $g;
        };
    }

    $closure = getClosure();
    $closure(1, 3); //13test
    echo "<br>";

    getClosure()->__invoke(1, 3); //13test
    echo "<br>";

    echo "<textarea rows='15' cols='50'>";
    var_dump($closure);
    echo "</textarea>";
    ?>
</small>

<p><b>Замечание:</b> Callback-функции, зарегистрированные такими функциями как call_user_func() и call_user_func_array(), не будут вызваны при наличии не пойманного исключения, брошенного в предыдущей callback-функции.</p>

</body>
</html>
