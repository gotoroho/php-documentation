<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/variables.php">Назад</a></li>
</ul>

<h1>$_FILES</h1>

<p>Ассоциативный массив (array) элементов, загруженных в текущий скрипт через метод HTTP POST. Структура этого массива описана в разделе Загрузка файлов методом POST.</p>

</body>
</html>
