<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Генераторы</h1>

<ul>
    <li><a href="/language/generators/overview.php">Знакомство с генераторами</a></li>
    <li><a href="/language/generators/syntax.php">Синтаксис генераторов</a></li>
    <li><a href="/language/generators/comparison.php">Сравнение генераторов с объектами класса Iterator</a></li>
</ul>

</body>
</html>
