<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Управляющие конструкции</h1>

<ul>
    <li><a href="/language/control-structures/intro.php">Введение</a></li>
    <li><a href="/language/control-structures/if.php">if</a></li>
    <li><a href="/language/control-structures/else.php">else</a></li>
    <li><a href="/language/control-structures/elseif.php">elseif/else if</a></li>
    <li><a href="/language/control-structures/alternative-syntax.php">Альтернативный синтаксис управляющих структур</a></li>
    <li><a href="/language/control-structures/while.php">while</a></li>
    <li><a href="/language/control-structures/do.while.php">do-while</a></li>
    <li><a href="/language/control-structures/for.php">for</a></li>
    <li><a href="/language/control-structures/foreach.php">foreach</a></li>
    <li><a href="/language/control-structures/break.php">break</a></li>
    <li><a href="/language/control-structures/continue.php">continue</a></li>
    <li><a href="/language/control-structures/switch.php">switch</a></li>
    <li><a href="/language/control-structures/match.php">match</a></li>
    <li><a href="/language/control-structures/declare.php">declare</a></li>
    <li><a href="/language/control-structures/return.php">return</a></li>
    <li><a href="/language/control-structures/require.php">require</a></li>
    <li><a href="/language/control-structures/include.php">include</a></li>
    <li><a href="/language/control-structures/require-once.php">require_once</a></li>
    <li><a href="/language/control-structures/include-once.php">include_once</a></li>
    <li><a href="/language/control-structures/goto.php">goto</a></li>
</ul>

</body>
</html>
