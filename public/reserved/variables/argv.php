<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/variables.php">Назад</a></li>
</ul>

<h1>$argv</h1>

<p>Содержит массив (array) всех аргументов, переданных скрипту при запуске из командной строки.</p>

<p><b>Замечание:</b> Первый аргумент $argv[0] всегда содержит имя файла запущенного скрипта.</p>

<p><b>Замечание:</b> Эта переменная недоступна, если register_argc_argv отключён.</p>

</body>
</html>
