<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/exceptions.php">Назад</a></li>
</ul>

<h1>ArithmeticError</h1>

<p>ArithmeticError выбрасывается, когда возникает ошибка при выполнении математических операций. Такие ошибки возможно спровоцировать побитовым смещением на отрицательное значение или вызовом функции intdiv(), приводящей значение, не входящее в допустимый интервал целых чисел (int).</p>

</body>
</html>
