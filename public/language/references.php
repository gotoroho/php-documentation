<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Атрибуты</h1>

<ul>
    <li><a href="/language/references/whatare.php">Что такое ссылки</a></li>
    <li><a href="/language/references/whatdo.php">Что делают ссылки</a></li>
    <li><a href="/language/references/arent.php">Чем ссылки не являются</a></li>
    <li><a href="/language/references/pass.php">Передача по ссылке</a></li>
    <li><a href="/language/references/return.php">Возврат по ссылке</a></li>
    <li><a href="/language/references/unset.php">Сброс переменных-ссылок</a></li>
    <li><a href="/language/references/spot.php">Неявное использование механизма ссылок</a></li>
</ul>

</body>
</html>
