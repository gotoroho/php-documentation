<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/interfaces.php">Назад</a></li>
</ul>

<h1>Класс WeakMap</h1>

<small>php 8</small>

<p>WeakMap - это коллекция (map) или словарь, который принимает объекты в качестве ключей. Однако, в отличие от аналогичного в остальном SplOb-jectStorage, объект в ключе WeakMap не влияет на счётчик ссылок объекта. То есть, если в какой-то момент единственной оставшейся ссылкой на объект является ключ WeakMap, объект будет собран сборщиком мусора и удалён из WeakMap. Его основной вариант использования - создание кешей данных, полученных из объекта, которым не нужно жить дольше, чем объект.</p>

<p>WeakMap реализует ArrayAccess, Iterator и Countable, поэтому в большинстве случаев его можно использовать так же, как ассоциативный массив.</p>

<pre>
    final class WeakMap implements Countable, ArrayAccess, IteratorAggregate {
        public __construct()
        public count(): int
        abstract public getIterator(): Traversable
        public offsetExists(object $object): bool
        public offsetGet(object $object): mixed
        public offsetSet(object $object, mixed $value): void
        public offsetUnset(object $object): void
    }
</pre>

<ul>
    <li>WeakMap::__construct — Создаёт новую коллекцию (map)</li>
    <li>WeakMap::count — Подсчитывает количество живых записей в коллекции (map)</li>
    <li>WeakMap::getIterator — Получает внешний итератор</li>
    <li>WeakMap::offsetExists — Проверяет, есть ли в коллекции (map) определённый объект</li>
    <li>WeakMap::offsetGet — Возвращает значение, на которое указывает определённый объект</li>
    <li>WeakMap::offsetSet — Обновляет коллекцию (map) новой парой ключ-значение</li>
    <li>WeakMap::offsetUnset — Удаляет запись из коллекции (map)</li>
</ul>

</body>
</html>
