<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Классы и объекты</h1>

<ul>
    <li><a href="/language/oop5/intro.php">Введение</a></li>
    <li><a href="/language/oop5/basic.php">Основы</a></li>
    <li><a href="/language/oop5/properties.php">Свойства</a></li>
    <li><a href="/language/oop5/constants.php">Константы классов</a></li>
    <li><a href="/language/oop5/autoload.php">Автоматическая загрузка классов</a></li>
    <li><a href="/language/oop5/decon.php">Конструкторы и деструкторы</a></li>
    <li><a href="/language/oop5/visibility.php">Область видимости</a></li>
    <li><a href="/language/oop5/inheritance.php">Наследование</a></li>
    <li><a href="/language/oop5/paamayim-nekudotayim.php">Оператор разрешения области видимости (::)</a></li>
    <li><a href="/language/oop5/static.php">Ключевое слово static</a></li>
    <li><a href="/language/oop5/abstract.php">Абстрактные классы</a></li>
    <li><a href="/language/oop5/interfaces.php">Интерфейсы объектов</a></li>
    <li><a href="/language/oop5/traits.php">Трейты</a></li>
    <li><a href="/language/oop5/anonymous.php">Анонимные классы</a></li>
    <li><a href="/language/oop5/overloading.php">Перегрузка</a></li>
    <li><a href="/language/oop5/iterations.php">Итераторы объектов</a></li>
    <li><a href="/language/oop5/magic.php">Магические методы</a></li>
    <li><a href="/language/oop5/final.php">Ключевое слово final</a></li>
    <li><a href="/language/oop5/cloning.php">Клонирование объектов</a></li>
    <li><a href="/language/oop5/object-comparison.php">Сравнение объектов</a></li>
    <li><a href="/language/oop5/late-static-bindings.php">Позднее статическое связывание</a></li>
    <li><a href="/language/oop5/references.php">Объекты и ссылки</a></li>
    <li><a href="/language/oop5/serialization.php">Сериализация объектов</a></li>
    <li><a href="/language/oop5/variance.php">Ковариантность и контравариантность</a></li>
</ul>

</body>
</html>
