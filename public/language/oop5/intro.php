<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Введение</h1>

<p>PHP включает полноценную объектную модель. Некоторые из её особенностей: видимость, абстрактные и ненаследуемые (final) классы и методы, а также магические методы, интерфейсы и клонирование.</p>

<p>PHP работает с объектами так же, как с ссылками или дескрипторами, это означает что каждая переменная содержит ссылку на объект, а не его копию. Более подробную информацию смотрите в разделе Объекты и ссылки.</p>

</body>
</html>
