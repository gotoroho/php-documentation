<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/variables.php">Назад</a></li>
</ul>

<h1>$GLOBALS</h1>

<p>$GLOBALS — Ссылки на все переменные глобальной области видимости</p>

<p>Ассоциативный массив (array), содержащий ссылки на все переменные, определённые в данный момент в глобальной области видимости скрипта. Имена переменных являются ключами массива.</p>

<pre>
    &lt;?php
    function test() {
        $foo = "локальная переменная";

        echo '$foo в глобальной области видимости: ' . $GLOBALS["foo"] . "\n";
        echo '$foo в текущей области видимости: ' . $foo . "\n";
    }

    $foo = "Пример содержимого";
    test();
    ?&gt;
</pre>

<small>
    $foo в глобальной области видимости: Пример содержимого<br>
    $foo в текущей области видимости: локальная переменная
</small>

<p><b>Замечание:</b> Это 'суперглобальная' или автоматическая глобальная переменная. Это просто означает, что она доступна во всех контекстах скрипта. Нет необходимости выполнять global $variable; для доступа к ней внутри метода или функции.</p>

<p><b>Замечание:</b> В отличие от всех остальных суперглобальных переменных, $GLOBALS всегда доступна в PHP.</p>

</body>
</html>
