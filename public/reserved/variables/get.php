<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/variables.php">Назад</a></li>
</ul>

<h1>$_GET</h1>

<p>$_GET — Переменные HTTP GET</p>

<p>Ассоциативный массив переменных, переданных скрипту через параметры URL (известные также как строка запроса). Обратите внимание, что массив не только заполняется для GET-запросов, а скорее для всех запросов со строкой запроса.</p>

<p><b>Замечание:</b> Это 'суперглобальная' или автоматическая глобальная переменная. Это просто означает, что она доступна во всех контекстах скрипта. Нет необходимости выполнять global $variable; для доступа к ней внутри метода или функции.</p>

<p><b>Замечание:</b> Параметры GET обрабатываются urldecode().</p>

</body>
</html>
