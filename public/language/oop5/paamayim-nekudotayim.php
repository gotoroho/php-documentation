<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Оператор разрешения области видимости (::)</h1>

<p>это лексема, позволяющая обращаться к статическим свойствам, константам и переопределённым свойствам или методам класса.</p>

<p>Для обращения к свойствам и методам внутри самого класса используются ключевые слова self, parent и static.</p>

</body>
</html>
