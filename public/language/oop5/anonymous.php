<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/oop5.php">Назад</a></li>
</ul>

<h1>Анонимные классы</h1>

<p>Это как класс, только без имени.</p>

<pre>
    ?php

    class SomeClass {}
    interface SomeInterface {}
    trait SomeTrait {}

    var_dump(new class(10) extends SomeClass implements SomeInterface {
        private $num;

        public function __construct($num)
        {
            $this->num = $num;
        }

        use SomeTrait;
    });

    ?&gt;
</pre>

<p><b>Замечание:</b> Обратите внимание, что анонимным классам присваиваются имена движком PHP, как показано в примере ниже. Это имя следует рассматривать как особенность реализации, на которую не следует полагаться.</p>

<pre>
    &lt;?php
    echo get_class(new class {});
</pre>

<small>
    class@anonymous/in/oNi1A0x7f8636ad2021
</small>

</body>
</html>
