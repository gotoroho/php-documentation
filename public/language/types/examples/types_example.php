<?php

class TypesExample
{
    private function multiPrint($name, $value)
    {
        echo "name: $name<br>" .
            "type: " . gettype($value) . "<br>" .
            "echo: " . $value . "<br>" .
            "var_dump: " . var_dump($value);
        echo "<hr>";
    }

    function main() {
        $vars = [
            "varBool" => true,
            "varInt" => 1,
            "varFloat" => 1.1,
            "varString" => "foo",
            "varArray" => ["foo" => "bar"],
            "varObject" => new MyObject(),
            "varCallable" => "callableFunction",
            "varIterable" => new MyIterable(),
            "varResource" => fopen("../test.txt", "r"),
            "varNull" => NULL,
        ];

        foreach ($vars as $name => $value) {
            $this->multiPrint($name, $value);
        }
    }
}

function callableFunction()
{
    return 1;
}

class MyIterable implements Iterator
{
    public function current()
    {
        //
    }

    public function next()
    {
        //
    }

    public function key()
    {
        //
    }

    public function valid()
    {
        //
    }

    public function rewind()
    {
        //
    }

    public function __toString()
    {
        return "MyIterable as a string";
    }
}

class MyObject
{
    public function __toString()
    {
        return "MyObject as a sting";
    }
}
