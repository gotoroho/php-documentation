<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
</ul>

<h1>Атрибуты</h1>

<ul>
    <li><a href="/language/attributes/overview.php">Введение в атрибуты</a></li>
    <li><a href="/language/attributes/syntax.php">Синтаксис атрибутов</a></li>
    <li><a href="/language/attributes/reflection.php">Чтение атрибутов с помощью Reflection API</a></li>
    <li><a href="/language/attributes/classes.php">Объявление классов атрибутов</a></li>
</ul>

</body>
</html>
