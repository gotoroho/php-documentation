<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/types.php">Назад</a></li>
</ul>

<h1>Объекты</h1>

<pre>
    &lt;?php
    $foo = new class() {
    };
    var_dump($foo);
    echo "&lt;br&gt;";

    $bar = new stdClass;
    var_dump($bar);
    echo "&lt;br&gt;";

    $array = (object)[
        "foo" => "bar",
        "test1",
        100 => "test2",
        ["test3"],
    ];
    var_dump($array);
    echo "&lt;br&gt;";

    $null = (object)null;
    var_dump($null);
    echo "&lt;br&gt;";

    $string = (object)"test test";
    var_dump($string);
    echo "&lt;br&gt;";

    $number = (object)123.456;
    var_dump($number);
    echo "&lt;br&gt;";
    ?&gt;
</pre>

<small>
    <?php
    $foo = new class() {
    };
    var_dump($foo);
    echo "<br>";

    $bar = new stdClass;
    var_dump($bar);
    echo "<br>";

    $array = (object)[
        "foo" => "bar",
        "test1",
        100 => "test2",
        ["test3"],
    ];
    var_dump($array);
    echo "<br>";

    $null = (object)null;
    var_dump($null);
    echo "<br>";

    $string = (object)"test test";
    var_dump($string);
    echo "<br>";

    $number = (object)123.456;
    var_dump($number);
    echo "<br>";
    ?>
</small>

</body>
</html>
