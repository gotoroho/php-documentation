<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/language/variables.php">Назад</a></li>
</ul>

<h1>Переменные переменных</h1>

<p>Переменная переменной берет значение переменной и рассматривает его как имя переменной.</p>

<pre>
    &lt;?php
    $a = 'hello';
    $$a = 'world';

    echo "$a ${$a}";    // hello world
    echo "$a $hello";   // hello world
</pre>

<p>Теперь в дереве символов PHP определены и содержатся две переменные: $a, содержащая "hello" и $hello, содержащая "world".</p>

<p>Для того чтобы использовать переменные переменных с массивами, вы должны решить проблему двусмысленности. То есть, если вы напишете $$a[1], обработчику необходимо знать, хотите ли вы использовать $a[1] в качестве переменной, либо вам нужна как переменная $$a, а затем её индекс [1]. Синтаксис для разрешения этой двусмысленности таков: ${$a[1]} для первого случая и ${$a}[1] для второго.</p>

<pre>
    &lt;?php
    class foo {
        var $bar = 'I am bar.';
        var $arr = array('I am A.', 'I am B.', 'I am C.');
        var $r   = 'I am r.';
    }

    $foo = new foo();
    $bar = 'bar';
    $baz = array('foo', 'bar', 'baz', 'quux');
    echo $foo->$bar . "\n";         // I am bar.
    echo $foo->{$baz[1]} . "\n";    // I am bar.

    $start = 'b';
    $end   = 'ar';
    echo $foo->{$start . $end} . "\n";  // I am bar.

    $arr = 'arr';
    echo $foo->{$arr[1]} . "\n";    // I am r.

    ?&gt;
</pre>

<p><b>Внимание</b> Переменные переменных не могут использоваться с суперглобальными массивами PHP. Переменная $this также является особой, на неё нельзя ссылаться динамически.</p>

</body>
</html>
