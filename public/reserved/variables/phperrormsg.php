<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    <li><a href="/">К оглавлению</a></li>
    <li><a href="/reserved/variables.php">Назад</a></li>
</ul>

<h1>$php_errormsg</h1>

<p><b>Внимание</b> Данная функциональность объявлена УСТАРЕВШЕЙ, начиная с PHP 7.2.0 и её крайне не рекомендуется использовать.</p>

</body>
</html>
